(function (global) {
	'use strict';
	/*jslint forin: true, nomen: true, regexp: true */

	function Method(method) {
		this.method = method;
	}

	Method.prototype = {

		getLength : function () {
			return this.getLines().length;
		},

		getLines : function () {

			var lines, methodStr = String(this.method);
			methodStr = methodStr.replace(/\/\*[\s\S]*?\*\//g, '');

			lines = methodStr.split('\n');

			lines = this._stripHead(lines);
			lines = this._stripEmptyAndSingleComments(lines);

			return lines;
		},

		_stripHead : function (lines) {
			if (/\s*\}\s*$/.test(lines[lines.length - 1])) {
				lines.pop();
			}
			if (/^\s*function\s*([$_a-zA-Z]\w*)?\s*\([^)]*\)\s*\{\s*$/.test(lines[0])) {
				lines.shift();
			}
			return lines;
		},

		_stripEmptyAndSingleComments : function (lines) {
			var i, len = lines.length;
			for (i = len; i>0; i-=1) {
				if (/^\s*(\/\/.*)?$/.test(lines[i])) {
					lines.splice(i, 1);
				}
			}
			return lines;
		},

		getArgsLength : function () {
//			return (this.method) ? this.method.length : -1;
			return this.method.length;
		},

		toObject : function () {
			return {
				length : this.getLength(),
				argsLength : this.getArgsLength(),
//				methodString : (this.method) ? this.method.toString() : "?"
				methodString : this.method.toString().substr(0,40)+"..."
			};
		}
	};

	function Kratko(object) {
		this.object = object;

		this.methods = {};

		this.stats = {
			totalMethodLength : 0,
			minMethodLength : Number.MAX_VALUE,
			maxMethodLength : 0,
			totalArgsLength : 0,
			minArgsLength : Number.MAX_VALUE,
			maxArgsLength : 0,
			numMethods : 0
		};

		this.collectMethods();
		this.collectStats();
	}

	Kratko.prototype = {

		getMethodNames : function () {
			var prop, names = [],
			_toString = Object.prototype.toString;
			for (prop in this.object) {
				if (_toString.call(this.object[prop]) === '[object Function]') {
					names.push(prop);
				}
			}
			return names;
		},

		collectMethods : function () {
			var self = this;
			this.methodNames = this.getMethodNames();
			this.methodNames.forEach(function(methodName) {
				self.methods[methodName] = new Method(self.object[methodName]);
			});
		},

		collectStats : function () {
			var methodName;
			if (this.methodNames.length === 0) {
				this.stats.minArgsLength =
					this.stats.minMethodLength =
					this.stats.averageArgsLength =
					this.stats.averageMethodLength = 0;

				return;
			}

			for (methodName in this.methods) {
				this.stats.numMethods+=1;
				this.getStatsForArgsLength(methodName);
				this.getStatsForMethodLength(methodName);
			}
			this.stats.averageArgsLength = parseFloat((this.stats.totalArgsLength / this.stats.numMethods).toFixed(2));
			this.stats.averageMethodLength = parseFloat((this.stats.totalMethodLength / this.stats.numMethods).toFixed(2));

			this.stats.medianMethodLength = this.getMedianForMethods();
			this.stats.medianArgsLength = this.getMedianForArgs();
		},

		getMedian : function (arr) {
			arr = arr.sort(function (a, b) {
					return a - b;
				});
			if (arr.length % 2) {
				return arr[Math.ceil(arr.length / 2)];
			} //else {
			return (arr[arr.length / 2 - 1] + arr[arr.length / 2]) / 2;
		},

		getMedianForMethods : function () {
			var methodName, methodLengths = [];
			for (methodName in this.methods) {
				methodLengths.push(this.methods[methodName].getLength());
			}
			return this.getMedian(methodLengths);
		},

		getMedianForArgs : function () {
			var methodName, argLengths = [];
			for (methodName in this.methods) {
				argLengths.push(this.methods[methodName].getArgsLength());
			}
			return this.getMedian(argLengths);
		},

		getStatsForArgsLength : function (methodName) {
			var argsLength = this.methods[methodName].getArgsLength();
			this.stats.totalArgsLength += argsLength;

			if (argsLength < this.stats.minArgsLength) {
				this.stats.minArgsLength = argsLength;
			}
			if (argsLength > this.stats.maxArgsLength) {
				this.stats.maxArgsLength = argsLength;
			}
		},

		getStatsForMethodLength : function (methodName) {
			var methodLength = this.methods[methodName].getLength();
			this.stats.totalMethodLength += methodLength;

			if (methodLength < this.stats.minMethodLength) {
				this.stats.minMethodLength = methodLength;
			}
			if (methodLength > this.stats.maxMethodLength) {
				this.stats.maxMethodLength = methodLength;
			}
		},

		getStats : function () {
			var prop, stats = {}, methods = {}, methodName;
			for (prop in this.stats) {
				stats[prop] = this.stats[prop];
			}
			for (methodName in this.methods) {
				methods[methodName] = this.methods[methodName].toObject();
			}
			stats.methods = methods;
			return stats;
		}
	};

	Kratko.getStatsFor = function (object) {
		return new Kratko(object).getStats();
	};
	
	Kratko.log = ["Kratko Log"];

	function showMethodSummary(method, methodName) {
		if(method.length>20 || method.argsLength>3) {
			var pad = (method.length>9) ? "" : "0";
			Kratko.log.push("lines: " + pad + method.length
				+ ", args: " + method.argsLength
				+ ", name: " + methodName 
			);
		}
	}
	
	
	Kratko.showBiggestMethods = function(object, label) {
		var maxLen, maxArgs, methodName, method,
				  stats = Kratko.getStatsFor(object);
		label = label || object.toString();
		//work out upper quartile
		maxLen = stats.maxMethodLength;
		maxArgs = stats.maxArgsLength;

//		console.log("--Methods with most lines or args: "+label);
		for(methodName in stats.methods) {
			method = stats.methods[methodName];
			if(method.length === maxLen || method.argsLength === maxArgs) {
				showMethodSummary(method, label + "." + methodName);
			}
		}
	};
	
	Kratko.showBiggestMethodsUQ = function(object, label) {
		var lenQ3, argQ3, methodName, method,
				  stats = Kratko.getStatsFor(object);
		label = label || object.toString();
		//work out upper quartile
		lenQ3 = (stats.medianMethodLength + stats.maxMethodLength) / 2;
		argQ3 = (stats.medianArgsLength + stats.maxArgsLength) / 2;
//		console.log("--Upper quartile lines or args: "+label);
		for(methodName in stats.methods) {
			method = stats.methods[methodName];
			if(method.length > lenQ3 || method.argsLength > argQ3) {
				showMethodSummary(method, label + "." + methodName);
			}
		}
	};


	global.Kratko = Kratko;

}(this));
