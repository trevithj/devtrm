// Karma configuration
// Generated on Wed Jul 17 2013 09:21:11 GMT+1200 (New Zealand Standard Time)

// base path, that will be used to resolve files and exclude
basePath = '..';

proxies =  {
 '/apacheBase/': 'http://localhost/GIT/devTRM/',
 '/tomcatBase/': 'http://localhost:8080/',
 'Home.html': 'http://localhost/GIT/devTRM/html/Home.html'
};


// list of files / patterns to load in the browser
files = [
  JASMINE,
  JASMINE_ADAPTER,
	//when using require, karma serves define() files ...
  REQUIRE,
  REQUIRE_ADAPTER,
	//so include any files here that need to be served.
	// included: false, [served: true (default)]
	{pattern: 'src/app/*.js', included: false},
	{pattern: 'src/base/*.js', included: false},
	{pattern: 'src/view/*.js', included: false},
	{pattern: 'src/model/*.js', included: false},

	//modify these to decide which subset of tests to run, then restart server
	{pattern: 'test/dev*Test.js', included: false},

	{pattern: 'lib/*.js', included: false},
	{pattern: 'data/*.json', included: false},
	{pattern: '/apacheBase/data/*.php', included: false},

	'test/config.require.js'

];

// list of files to exclude (note: doesn't like patterns)
exclude = [
	'test/trmDataNodeTest.js',
	'test/config.karma.dev.js'
];


// test results reporter to use
// possible values: 'dots', 'progress', 'junit'
reporters = ['progress'];


// web server port
port = 9876;


// cli runner port
runnerPort = 9100;


// enable / disable colors in the output (reporters and logs)
colors = true;


// level of logging
// possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
logLevel = LOG_INFO;


// enable / disable watching file and executing tests whenever any file changes
autoWatch = true;


// Start these browsers, currently available:
// - Chrome
// - ChromeCanary
// - Firefox
// - Opera
// - Safari (only Mac)
// - PhantomJS
// - IE (only Windows)
browsers = [];

// If browser does not capture in given timeout [ms], kill it
captureTimeout = 16000;


// Continuous Integration mode
// if true, it capture browsers, run tests and exit
singleRun = false;
//singleRun = true;
