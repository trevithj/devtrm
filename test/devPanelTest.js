define(['jquery', 'Panel', 'd3'], function($, Panel) {
	'use strict';
	/*globals describe, beforeEach, afterEach, it, expect, jasmine */
	var theDiv= null, panel = null, mainID = 'mainPanel';

	describe("Panel instance", function() {

		beforeEach(function() {
			theDiv = document.createElement('div');
			theDiv.id = mainID;
			$(theDiv).width(100); //0-99
			$(theDiv).height(100);
			document.body.appendChild(theDiv);
			panel = new Panel(mainID);
		});

		afterEach(function() {
			var div = document.getElementById(mainID);
			document.body.removeChild(div);
			panel = null;
		});

		it("has been correctly started", function() {
			expect(theDiv).toBeDefined();
			expect(panel).toBeDefined();
			var w = $(theDiv).width(), h = $(theDiv).height();
			expect(panel.xScale().range()).toEqual([0,w-1]);
			expect(panel.yScale().range()).toEqual([0, h-1]);
		});
		it("has created theDiv", function() {
			var div = panel.theDiv();
			expect(div).toBeDefined();
			expect(div.width).toBeDefined();
			expect(div.width()).toBeGreaterThan(0);
			expect(panel.width()).toEqual(div.width());
			expect(panel.height()).toEqual(div.height());
		});
		it("has populated theDiv", function() {
			//var div = panel.theDiv();
			var sub = panel.theBG();
			expect(sub).toBeDefined();
			expect(sub.empty).toBeDefined();
			expect(sub.empty()).toEqual(false);
			sub = panel.theSVG();
			expect(sub).toBeDefined();
			expect(sub.empty).toBeDefined();
			expect(sub.empty()).toEqual(false);
			sub = panel.theVis();
			expect(sub).toBeDefined();
			expect(sub.empty).toBeDefined();
			expect(sub.empty()).toEqual(false);
		});
		it("has useful dimensions", function() {
			panel.xScale().domain([0,7]);
			panel.yScale().domain([0,7]);
			var dim = panel.getDimensions();
			expect(dim).toBeDefined();
			expect(dim.x1).toEqual(0);
			expect(dim.y1).toEqual(0);
			expect(dim.x2).toBeCloseTo(7, 0.00000001); //allow for rounding errors
			expect(dim.y2).toBeCloseTo(7, 0.00000001);
		});
		it("can distinguish zero from undefined", function() {
			var x = panel.xScale(0);
			expect(isNaN(x)).toEqual(false);
			expect(x).toEqual(jasmine.any(Number));
			x = panel.yScale(0);
			expect(isNaN(x)).toEqual(false);
			expect(x).toEqual(jasmine.any(Number));
			x = panel.xScale();
			expect(isNaN(x)).toEqual(true);
			expect(x(3)).toEqual(jasmine.any(Number));
			x = panel.yScale();
			expect(isNaN(x)).toEqual(true);
			expect(x(3)).toEqual(jasmine.any(Number));
		});

	});

});
