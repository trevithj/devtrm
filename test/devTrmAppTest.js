define(['src/app/App', 'jquery', 'mohio', 'kratko', 'd3'],
 function(makeApp, $, mohio) {
	'use strict';
	/*globals define, describe, it, expect, dump, d3, runs, waitsFor Kratko */
	var result = null,
		theDiv = document.createElement('div'),
		theNav = document.createElement('div');
	theDiv.id = "mohio-main"; //hard-coded
	theNav.id = "nav-bar"; //hard-coded
	document.body.appendChild(theDiv);
	document.body.appendChild(theNav);
	//Start me up
	mohio = makeApp(mohio);
	mohio.trm.init();
	mohio.bind("graphLoaded", function(res) {
		result = res; //set locally for testing
	});
//	mohio.trm.load('base/data/testGraphFull.json');
	mohio.trm.load(null, 'apacheBase/data/read.php');
	Kratko.showBiggestMethods(mohio, "mohio");
	Kratko.showBiggestMethods(mohio.core, "mohio.core");
	Kratko.showBiggestMethods(mohio.table, "mohio.table");
	Kratko.showBiggestMethods(mohio.trm, "trm");
	Kratko.showBiggestMethods(mohio.trm.control, "trm.control");
	Kratko.showBiggestMethods(mohio.trm.graph, "trm.graph");
	Kratko.showBiggestMethods(mohio.trm.matrix, "trm.matrix");
	Kratko.showBiggestMethods(mohio.trm.matrix1, "trm.matrix1");
	Kratko.showBiggestMethods(mohio.trm.matrix2, "trm.matrix2");
	Kratko.showBiggestMethods(mohio.trm.matrix3, "trm.matrix3");
	Kratko.showBiggestMethods(mohio.trm.mindMap, "trm.mindMap");
	Kratko.showBiggestMethods(mohio.trm.mindMap1, "trm.mindMap1");
	Kratko.showBiggestMethods(mohio.trm.mindMap2, "trm.mindMap2");
	Kratko.showBiggestMethods(mohio.trm.mindMap3, "trm.mindMap3");
	Kratko.showBiggestMethods(mohio.trm.planner, "trm.planner");
	Kratko.showBiggestMethods(mohio.trm.roadmap, "trm.roadmap");
	Kratko.showBiggestMethods(mohio.trm.segments, "trm.segments");
	Kratko.showBiggestMethods(mohio.trm.tabPanel, "trm.tabPanel");
	console.log(Kratko.log);

	//And test me
	//	mohio.debugOn=true;
	function getById(id) {
		var el = document.getElementById(id);
		return el || {};
	}

	describe("RoadMapApp", function() {
		it("adds the app object to mohio", function() {
			expect(mohio.trm).toBeDefined();
			expect(mohio.trm).not.toBeNull();
		});
		it("creates all the required divs", function() {
			var div, ids = mohio.trm.divIDs;
			ids.forEach(function(id, i) {
				div = getById(id);
				expect(div.id).toEqual(ids[i]);
			});
			expect(mohio.trm.tabDivs[0].length).toEqual(ids.length);
		});

		it("creates all the required panels", function() {
			var views = mohio.trm.mindMap.views,
			panelCount = mohio.trm.divMapPanels.length;
			expect(views.keys().length).toEqual(panelCount);
			expect(views.values().length).toEqual(panelCount);
		});

		it("allows map panels to be selected by divID", function() {
			var index = mohio.trm.divMapPanels[0],
				view = mohio.trm.mindMap.views.get(mohio.trm.divIDs[index]);
			expect(view.panel.theDiv()).toEqual($("#"+mohio.trm.divIDs[index]+"-panel"));
		});

		it("can hide or show the selected divs", function() {
			mohio.trm.tabDivs.each(function() {
				expect(d3.select(this).attr("display")).not.toEqual("none");
			});
			mohio.trm.hideOtherDivs("unknownID");
			mohio.trm.tabDivs.each(function() {
				var div = d3.select(this);
				if(div.attr("id") === "divTabs") {
					expect(div.style("display")).not.toEqual("none");
				} else {
					expect(div.style("display")).toEqual("none");
				}
			});
		});
	});
	
	function getMapDivID(mapNum) {
		var index = mohio.trm.divMapPanels[mapNum];
		return mohio.trm.divIDs[index];
	}
	function getMatrixDivID(mxNum) {
		var index = mohio.trm.divMatrixDivs[mxNum];
		return mohio.trm.divIDs[index];
	}
	
	function doTableClick(divID) {
		var btn = d3.select("#"+divID+"-table").select("button");
		btn.on("click")();
	}
	
	describe("Graph data", function() {
		waitsFor(function() {
			return (result !== null);
		}, 3000);

//		runs(function() {
		describe("mohio.core.load()", function() {
			var data;
			it("has returned data without error", function() {
				expect(mohio.trm.graph).toBeDefined();
				expect(result.error).toBeNull();
				expect(result.data).not.toBeNull();
				data = result.data;
			});
			it("has initialized graph", function() {
				var graph = mohio.trm.graph;
				expect(graph.nodes.length).toEqual(data.nodes.length);
			});
			it("has standard map children", function() {
				var edges = mohio.trm.graph.getEdges("MMap123.1", true, true);
				expect(edges.length).toEqual(4);
			});
		});

		describe("User ADD action", function() {
			var data, nodeCount, edgeCount;
			it("has loaded initial data", function() {
				expect(result.data).not.toBeNull();
				data = result.data;
			});
			it("adds nodes to the graph correctly after adding drivers", function() {
				nodeCount = data.nodes.length;
				edgeCount = data.edges.length;
				var driverDivID = getMapDivID(0);
				mohio.trm.mindMap1.show(driverDivID);
				doTableClick(driverDivID); //add two drivers
				doTableClick(driverDivID);
				expect(data.nodes.length).toEqual(nodeCount+2);
				expect(data.edges.length).toEqual(edgeCount);
			});
			it("adds nodes to the graph correctly after adding segments", function() {
				nodeCount = data.nodes.length;
				edgeCount = data.edges.length;
				var segmtDivID = "divSegments";
				mohio.trm.segments.show(segmtDivID);
				doTableClick(segmtDivID); //add one segment
				expect(data.nodes.length).toEqual(nodeCount+1);
				expect(data.edges.length).toEqual(edgeCount+1);//Segments - segment
			});
			it("adds edges to the graph correctly after viewing matrix1", function() {
				var driverNodes = mohio.trm.graph.getNodes("driver");
				var segmntNodes = mohio.trm.graph.getNodes("segment");
				var segDrvEdges = mohio.trm.graph.getEdgesByFieldValue("relType", ["seg-driver"]);
				var expected = driverNodes.length * segmntNodes.length;
				expect(segDrvEdges.length).toBeLessThan(expected); //new edges not added yet
				var diff = expected - segDrvEdges.length;
				var mx1DivID = mohio.trm.divIDs[2];
				edgeCount = data.edges.length;
				mohio.trm.matrix1.show(mx1DivID);
				segDrvEdges = mohio.trm.graph.getEdgesByFieldValue("relType", ["seg-driver"]);
				expect(segDrvEdges.length).toEqual(expected);
				expect(data.edges.length).toEqual(edgeCount+diff);
			});
		});
	
//		describe("Planner", function() {
//			var divID = "divPlanner", div = d3.select("#"+divID);
//			mohio.trm.planner.show(divID);
//			it("displays a list of current solutions", function() {
//				var tbls = div.selectAll("table");
//				expect(tbls.empty()).toEqual(false);
//				expect(tbls[0].length).toEqual(3);
//			});
//			it("displays a list of related tasks", function() {
//				//click a solution row
//				//check contents of the task table
//			});
//			it("displays a list of current resources", function() {
//			});
//		});
	});


//	describe("trm.matrix", function() {
//		it("", function() {
//			
//		});
//	});
});
