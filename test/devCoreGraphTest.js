define(['CoreGraph', 'DataEdge'],function(CoreGraph, DataEdge){
	'use strict';
	/*globals d3, it, describe, expect, dump, beforeEach */
	var graph, data;//, errorNode = null;
//	mohio.bind("newGraphProcessing", function(){});
//	mohio.bind("processNewNodeError", function(node){ errorNode = node;});
	function addNodes(guids, vType) {
		graph = graph || new CoreGraph();
		vType = vType || "default";
		guids.forEach(function(guid) {
			graph.addNode(guid, "Test", vType);
		});
	}

	function addTargets(srcGUID, tgtGUIDs, relType) {
		relType = relType || "default";
		tgtGUIDs.forEach(function(tgtGUID) {
			graph.addEdge(srcGUID, tgtGUID, relType);
		});
	}

	describe("The core Graph object", function() {
		beforeEach(function() {
			graph = new CoreGraph();
		});
		it("can create edge GUIDs", function() {
			var src={GUID:"A"}, tgt={GUID:"B"},
				guid1 = CoreGraph.defaultEdgeGUID(src.GUID, tgt.GUID),
				guid2 = CoreGraph.defaultEdgeGUID(src, tgt),
				guid3 = CoreGraph.defaultEdgeGUID({sourceGUID:"A", targetGUID:"B"});
				expect(guid1).toBeDefined();
				expect(guid1).toEqual(guid2);
				expect(guid1).toEqual(guid3);
		});

		it("instantiates the nodes and links arrays", function() {
			expect(graph.nodes).toEqual([]);
			expect(graph.edges()).toEqual([]);
		});

		it("can recognise extraProperties", function() {
			graph.addNode("TheGUID", "Test", "default", {"test":123, "array":[1,2,3]});
			var node = graph.nodes[0];
			expect(node.test).toEqual(123);
			expect(node.array).toEqual([1,2,3]);
		});

		it("can add and delete nodes", function() {
			var i, n = 10;
			expect(graph.nodes.length).toEqual(0);
			for(i=0; i<n; i+=1) {
				graph.addNode("TheGUID"+i, "Test", "default");
			}
			expect(graph.nodes.length).toEqual(n);
			for(i=0; i<n; i+=1) {
				graph.deleteNode("TheGUID"+i);
			}
			expect(graph.nodes.length).toEqual(0);
		});

		it("can add and delete edges", function() {
			addNodes(["nodeA", "nodeB"]);
			graph.addEdge("nodeA", "nodeB", "test");
			graph.addEdge("nodeB", "nodeA", "test");
			expect(graph.edges().length).toEqual(2);
			graph.deleteEdge("nodeA", "nodeB");
			expect(graph.edges().length).toEqual(1);
		});
		it("deletes all parallel edges at once", function() {
			addNodes(["nodeA", "nodeB"]);
			graph.addEdge("nodeA", "nodeB", "test1");
			graph.addEdge("nodeA", "nodeB", "test2");
			graph.addEdge("nodeA", "nodeB", "test3");
			expect(graph.edges().length).toEqual(3);
			graph.deleteEdge("nodeA", "nodeB");
			expect(graph.edges().length).toEqual(0);
		});

		it("can find nodes of a given visualType", function() {
			addNodes(["nodeA", "nodeD"], "default");
			addNodes(["nodeB", "nodeC"], "target");
			//graph.getNodes("target", nodes);
			var nodes = graph.getNodes("target", ["Already here"]);
			expect(nodes.length).toEqual(3);
		});

		it("can find nodes with a given arbitrary property", function() {
			addNodes(["nodeA", "nodeD"], "default");
			addNodes(["nodeB", "nodeC"], "target");
			//graph.getNodes("target", nodes);
			var nodes = graph.getNodes(function(n) {
				return n.GUID.indexOf("B") > -1;
			});
			expect(nodes.length).toEqual(1);
			expect(nodes[0].GUID).toEqual("nodeB");
		});

		it("can get sources and targets of a given node", function() {
			addNodes(["nodeA", "nodeB","nodeC"], "default");
			graph.addEdge("nodeA", "nodeB", "");
			graph.addEdge("nodeA", "nodeC", "");
			var srcs = [], tgts = [];
			graph.getSources("nodeC", srcs);
			expect(srcs.length).toEqual(1);
			expect(srcs[0].GUID).toEqual("nodeA");
			graph.getTargets("nodeA", tgts);
			expect(tgts.length).toEqual(2);
		});


		it("can compare itself to another graph", function() {});

		it("can extract a list of all semantic types", function() {
			var list, node, stypes = ["A","B"];
			graph.addNode("nodeA", "Test", "default", {semanticTypes:stypes});
			node = graph.nodes[0];
			expect(node.semanticTypes).toEqual(stypes);
			graph.addNode("nodeB", "Test", "default", {semanticTypes:["A", "B","C"]});
			graph.addNode("nodeC", "Test", "default", {semanticTypes:["A"]});
			list = graph.getSemanticTypesList();
			expect(list[0].type).toEqual(stypes[0]);
			expect(list[0].count).toEqual(3);
		});


	});

	describe("CoreGraph.newGraph", function() {
		beforeEach(function() {
			graph = new CoreGraph();
			data = null;
		});
		it("rejects bad data", function() {
			expect(function() {	graph.newGraph(data, null);	}).toThrow("graph must be defined");
			data = {edges:[]};	//no nodes
			expect(function() {	graph.newGraph(data, null);	}).toThrow("nodes must be defined");
			data = {nodes:[]};	//no edges
			expect(function() {	graph.newGraph(data, null);	}).toThrow("edges must be defined");
			data = {nodes:[{}], edges:[{}]};	//no GUIDs
			expect(function() {	graph.newGraph(data, null);	}).toThrow("GUID must be defined");
			data = {nodes:[{GUID:"node0"}, {GUID:"node1"}], edges:[{sourceGUID:"node0",targetGUID:"node1"}]};
			expect(function() {	graph.newGraph(data, null);	}).toThrow("centerNode must be set");
		});

		it("processes arbitrary data as nodes", function() {
			data = {nodes:[{GUID:"node0", centerNode:true}, {GUID:"node1"}], edges:[{sourceGUID:"node0",targetGUID:"node1"}]};
			expect(graph.nodes.length).toEqual(0);
			expect(graph.edges().length).toEqual(0);
			expect(function() {	graph.newGraph(data); }).not.toThrow();
			expect(function() {	graph.newGraph(data, null); }).not.toThrow();
			expect(graph.nodes.length).toEqual(data.nodes.length);
			expect(graph.edges().length).toEqual(data.edges.length);
			expect(function() {	graph.newGraph(data, "node1"); }).not.toThrow();
			expect(function() {	graph.newGraph(data, "nodeX"); }).toThrow();
		});
	});

	describe("CoreGraph.processNewNode", function() {
		it("rejects nodes that are not DataNode instances", function() {
			graph = new CoreGraph();
			data = {GUID:"123"};
			expect(function() {	graph.processNewNode(data);	}).toThrow("Expected DataNode instance");
		});
	});

	describe("CoreGraph.processNewEdge", function() {
		var n0, n1, edge;
		beforeEach(function() {
			graph = new CoreGraph();
			n0 = graph.addNode("n0");
			n1 = graph.addNode("n1");
		});
		function addEdge() {
			edge = graph.processNewEdge(new DataEdge(data));
		}

		it("rejects edges with undefined src/tgt nodes", function() {
			data = {sourceGUID:"nx",targetGUID:"n1", GUID:"XX"};
			expect(addEdge).toThrow("Source node undefined for edge:XX");
			data = {sourceGUID:"n0",targetGUID:"nx", GUID:"XX"};
			expect(addEdge).toThrow("Target node undefined for edge:XX");
			data = {sourceGUID:"n0",targetGUID:"n1", GUID:"XX"};
			expect(addEdge).not.toThrow();
			expect(edge.source).toBe(n0);
			expect(edge.target).toBe(n1);
		});
		it("defaults edge visibility to true", function() {
			data = {sourceGUID:"n0",targetGUID:"n1"};
			addEdge();
			expect(edge.visible).toEqual(true);
			data = {sourceGUID:"n0",targetGUID:"n1", visible:null};
			addEdge();
			expect(edge.visible).toEqual(true);
		});
		it("sets outEdges based on visibility", function() {
			data = {sourceGUID:"n0",targetGUID:"n1", visible:false};
			addEdge();
			expect(n0.outEdges.length).toEqual(0);
			expect(n0._outEdges.length).toEqual(1);
			n0._outEdges = [];
			data = {sourceGUID:"n0",targetGUID:"n1", visible:true};
			addEdge();
			expect(n0._outEdges.length).toEqual(0);
			expect(n0.outEdges.length).toEqual(1);
		});
	});

	describe("CoreGraph.addNode", function() {
		var node;
		beforeEach(function() {
			graph = new CoreGraph();
			node = graph.addNode("123", "title", "vType");
		});
		it("rejects duplicate GUIDs", function() {
			expect(node.GUID).toEqual("123");
			expect(function() {	graph.addNode("123");	}).toThrow('GUID already in use: 123');
			expect(function() {	graph.addNode();	}).toThrow('GUID must be defined');
		});
		it("rejects blank GUIDs", function() {
			expect(function() {	graph.addNode();	}).toThrow('GUID must be defined');
		});
		it("sets default title and visualType", function() {
			expect(node.title).toEqual("title");
			expect(node.visualType).toEqual("vType");
			node = graph.addNode("TheGUID");
			expect(node.title).toEqual("TheGUID");
			expect(node.visualType).toEqual("_default");
		});
		it("uses different underlying data instances", function() {
			var data = {"custom": 123};
			node = graph.addNode("new123", "title", "vType", data);
			expect(node.custom).toEqual(123);
			data.custom = 456;
			data.newField="hello";
			expect(node.custom).not.toEqual(456);
			expect(node.newField).toBeUndefined();
		});
	});

	describe("Populated CoreGraph", function() {
		beforeEach(function() {
			graph = new CoreGraph();
			addNodes(["nodeA", "nodeB", "nodeC","nodeD", "nodeE"]);
			addTargets("nodeA", ["nodeB", "nodeC"], "a-x");
			addTargets("nodeB", ["nodeA", "nodeC", "nodeD"], "b-x");
			addTargets("nodeE", ["nodeA", "nodeB"]);
		});

		it("can return arbitrarily-filtered array of edges", function(){
			var edges = graph.edges(function(link){
				return (link.targetGUID ==="nodeC");
			});
			expect(edges.length).toEqual(2);
			edges = graph.edges(function(link){
				return (link.relType.indexOf("-x") > -1);
			});
			expect(edges.length).toEqual(5);
		});

		it("can return spanning subtree using node.dfs()", function() {
			var node = graph.nodeMap.nodeA,
			visited = [];
			node.dfs(function(n) {
				if(visited.indexOf(n.GUID) > -1) {
					return "alreadyVisited";
				} //else
				visited.push(n.GUID);
				return "visitOK";
			});
			expect(visited[0]).toEqual("nodeA");
			expect(visited[1]).toEqual("nodeB");
			expect(visited[2]).toEqual("nodeC");
			expect(visited[3]).toEqual("nodeD");
			expect(visited.length).toEqual(4);
		});

		it("can return spanning subtree using edge.dfs()", function() {
			var edge = graph.findLink("nodeE","nodeA"),
			visited = [];
			edge.dfs(function(e) {
				if(visited.indexOf(e.GUID) > -1) {
					return "alreadyVisited";
				} //else
				visited.push(e.GUID);
				return "visitOK";
			});
			
		});
	});

});
