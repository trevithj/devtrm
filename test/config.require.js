/** as per instruction on:
* http://karma-runner.github.io/0.8/plus/RequireJS.html
*/
var tests = Object.keys(window.__karma__.files).filter(function (file) {
	return /Test\.js$/.test(file);
});
//dump(tests);

require.config({
	baseUrl: '/base',
	paths: {
		'jquery': 'lib/jquery-1.7.1',
		'mohio': 'src/base/Mohio',
		'coreMohio': 'src/base/Mohio.core',
		'DataEdge': 'src/model/DataEdge',
		'DataNode': 'src/model/DataNode',
		'VisualEdge': 'src/view/VisualEdge',
		'VisualNode': 'src/view/VisualNode',
		'CoreGraph': 'src/model/Graph.core',
		'MatrixGraph': 'src/model/Graph.matrix',
		'Panel': 'src/view/Panel',
		'start': 'src/app/start',
		'd3': 'lib/d3',
		'kratko': 'lib/kratko' //code refactoring tool
		},
	// ask Require.js to load these files (all our tests)
	deps: tests,
	// start test run, once Require.js is done
	callback: window.__karma__.start
});
