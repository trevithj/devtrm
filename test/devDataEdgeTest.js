define(['DataEdge'],function(DataEdge){
	'use strict';
	/*globals d3, it, describe, expect, dump, beforeEach */
	var data, edge;

	describe("DataEdge", function() {
		var n0, n1;
		beforeEach(function() {
			n0 = {outEdges:[], _outEdges:[]};
			n1 = {inEdges:[]};
			data = {sourceGUID:"n0",targetGUID:"n1"};
		});
		function makeEdge(d) {
			d = d || data;
			return new DataEdge(d);
		}		
		it("rejects edges with undefined src/tgt GUIDs", function() {
			data = {sourceGUID:"nx"};
			expect(makeEdge).toThrow("Required GUID missing in parameters");
			data = {targetGUID:"nx"};
			expect(makeEdge).toThrow("Required GUID missing in parameters");
			data = {sourceGUID:"n0",targetGUID:"n1"};
			expect(makeEdge).not.toThrow();
		});
		it("defaults edge visibility to true unless explicitly set to false", function() {
			edge = makeEdge();
			expect(edge.visible).toEqual(true);
			data.visible=null;
			edge = makeEdge();
			expect(edge.visible).toEqual(true);
			data = {sourceGUID:"n0",targetGUID:"n1", visible:false};
			edge = makeEdge();
			expect(edge.visible).toEqual(false);
		});
		it("will inherit parent properties as expected", function() {
			data.custom = "parent value";
			edge = makeEdge();
			expect(edge.custom).toBeDefined();
		});
		it("can read original parent data properties", function() {
			data.custom = "A custom data value";
			edge = makeEdge();
			expect(edge.getData("custom")).toEqual("A custom data value");
		});
		it("can separately update parent data properties", function() {
			data.custom = "parent value";
			edge = makeEdge();
			expect(data.custom).toEqual("parent value");
			edge.setData("custom", "newval");
			expect(data.custom).toEqual("newval");
		});
		it("will reflect changes in parent data if property isn't overridden", function() {
			data.custom = "parent value";
			edge = makeEdge();
			expect(edge.custom).toEqual("parent value");
			data.custom = "new value";
			expect(edge.custom).toEqual("new value");
		});
		it("will ignore changes in parent data if property is overridden", function() {
			data.custom = "parent value";
			edge = makeEdge();
			expect(edge.custom).toEqual("parent value");
			edge.custom = "local value";
			data.custom = "new parent value";
			expect(edge.custom).toEqual("local value");
			expect(edge.getData("custom")).toEqual("new parent value");
		});

	});
});