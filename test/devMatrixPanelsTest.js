/**
*/
define(['src/app/Matrix', 'd3'],
function(addMatrix) {
	'use strict';
	/*globals d3: false, it: false, describe: false, expect: false, dump: false */
	var trm = addMatrix({});
/**
	describe("Matrix", function() {
		it("has generic functions", function() {
		});
	});
	describe("Matrix1", function() {
		it("has specific functions for matrix1", function() {
		});
	});
	describe("Matrix2", function() {
		it("has specific functions for matrix2", function() {
		});
	});
	describe("Matrix3", function() {
		it("has specific functions for matrix3", function() {
		});
	});
**/

	function toEdges(vals) {
		vals.forEach(function(val, i) {
			vals[i] = { 
				wt: vals[i],
				getData: function() { return this.wt; },
				setData: function(nm, v) { this.wt = v; }
			};
		});
		return vals;
	}

	describe("Matrix2.testCalc", function() {
		it("returns array of normalized values", function() {
			var sRow = toEdges([1,2]), 
				rGrid=[
					toEdges([1,1]),
					toEdges([2,2]),
					toEdges([1,1])
				],
				calcCol = toEdges([0,0,0]),
				expected = [5,10,5];
			calcCol = trm.matrix2.testCalc(sRow, rGrid, calcCol);
			calcCol.forEach(function(e, i) {
				expect(e.getData()).toEqual(expected[i]);
			});
		});
	});
/**/
});

