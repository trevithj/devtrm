define(['src/app/MindMap', 'd3'], function(addMindMap) {
	'use strict';
	/*globals describe, beforeEach, afterEach, it, expect, jasmine */
	var trm = {}, theDiv= null, mainID = 'mainPanel';
	trm.divIDs = ["bogus", mainID, "bogus"];
	trm.panels = d3.map();
	trm = addMindMap(trm);

	describe("MindMap instance", function() {

		beforeEach(function() {
			theDiv = document.createElement('div');
			theDiv.id = mainID;
			$(theDiv).width(100); //0-99
			$(theDiv).height(100);
			document.body.appendChild(theDiv);
		});

		afterEach(function() {
			var div = document.getElementById(mainID);
			document.body.removeChild(div);
		});

		it("defines a map view object", function() {
			trm.mindMap.makeViews([1]); //only use mainID
//			var mapPanel = trm.panels.get(mainID);
			var mapView = trm.mindMap.views.get(mainID);
			expect(mapView.panel).toBeDefined();
			expect(mapView.panelDiv).toBeDefined();
			expect(mapView.table).toBeDefined();
			expect(mapView.tableDiv).toBeDefined();
		});

	});

});
