


define(['jquery', 'mohio', 'd3'],
function($, mohio) {
'use strict';
/*globals define:false, describe:false, it:false, expect:false,
 dump:false, d3:false */
 
	function getById(id) {
		var el = document.getElementById(id);
		return (el) ? el : {};
	}

	describe("mohio.core.makeDivs()", function() {
		beforeEach(function() {
			//$("body").html("");
		});
		it("creates all the required divs", function() {
			var div, ids = ["abc", "def", "g123"];
			mohio.core.makeDivs(ids);
			ids.forEach(function(id, i) {
				div = getById(id);
				expect(div).not.toBeNull();
				expect(div.id).toEqual(ids[i]);
			});
		});

		it("doesn't overwrite existing divs", function() {
			var divs, ids = ["abc", "def", "g123"];
			$("body").append("<div id='"+ids[1]+"'></div>");
			getById(ids[1]).innerHTML = "<p>Stay with me</p>";

			divs = mohio.core.makeDivs(ids);
			expect(divs[0].length).toEqual(3);
			expect(divs[0][1].innerHTML).toEqual("<p>Stay with me</p>");
		});

		it("can be called multiple times", function() {
			var divsAll = null, parent = d3.select("#testDiv");
			parent = parent.empty() ? d3.select("body").append("div").attr("id", "testDiv") : parent;
			mohio.core.makeDivs(["abc", "def"], parent);
			mohio.core.makeDivs(["g123"], parent);
			divsAll = parent.selectAll("div");
			expect(divsAll[0].length).toEqual(3);
					/*
			runs(function() {
				setTimeout(function() {
					$("#body").html("");
					dump("clearing html");
					mohio.core.makeDivs(["abc", "def"]);
					mohio.core.makeDivs(["g123"]);
					divsAll = d3.selectAll("div");
				}, 1000);
			});
			waitsFor(function() {
				return divsAll !== null;
			});
			runs(function() {
				expect(divsAll[0].length).toEqual(3);
			});*/
		});
	});

});
