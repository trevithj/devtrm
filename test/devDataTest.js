/*
Testing of both CoreGraph's data handling ability, and the testFullGraph structure's validity
for representing a full end-to-end usecase of the TRM-roadmap process.
*/
define(['mohio', 'CoreGraph', 'd3'],
function(mohio, CoreGraph) {
	'use strict';
	/*globals d3: false, it: false, describe: false, expect: false, dump: false */

	var rawData = null, error = null, graph = new CoreGraph(),
//	server = "base/data/testGraphFull.json";
//	server = "/apacheBase/data/read.php?file=testGraphFull.default";
	server = "/apacheBase/data/read.php?ver="+new Date().getTime(); //avoid cacheing

	mohio.core.load(server, function (result) {
		rawData = result.data;
		error = result.error;
	});
	
	describe("Asynch-loaded", function(){
		waitsFor(function() {
			return rawData !== null;
		}, 1000);
		
//		runs(function() {
		describe("CoreGraph", function() {
			it("has valid json to work with", function() {
				expect(error).toBeNull();
				expect(rawData.nodes).toBeDefined();
				expect(rawData.edges).toBeDefined();
				expect(rawData.rootID).toBeDefined();
			});
			it("has loaded the json graph", function() {
				graph.newGraph(rawData, rawData.rootID);
				expect(graph.nodes.length).toEqual(rawData.nodes.length);
				expect(graph.edges().length).toEqual(rawData.edges.length);
				expect(graph.root.GUID).toEqual(rawData.rootID);
			});
		});

		describe("Core data", function() {
			it("has 5 sub-root nodes that reference the project root", function() {
				var names, vTypes, rootID = graph.root.GUID,
				 nodes = graph.getNodes(function(node) {
					return (node.parent === undefined) ?  false : (node.parent === rootID);
				});
				names = [], vTypes = [];
				nodes.forEach(function(node) {
					names.push(node.name);
					vTypes.push(node.visualType);
				});
				names.sort();
				expect(names).toEqual(["Market Segments","MindMap 1 - drivers","MindMap 2 - features","MindMap 3 - solutions","Tactical Planner"]);
				vTypes.sort();
				expect(vTypes).toEqual(["map1Root","map2Root","map3Root","planRoot","segRoot"]);
			});
			it("ensures that any driver nodes have an index property", function() {
				var nodes = graph.getNodes("driver");
				nodes.forEach(function(node) {
					expect(node.index).toEqual(jasmine.any(Number));
				});
			});
			it("ensures that any feature nodes have an index property", function() {
				var nodes = graph.getNodes("feature");
				nodes.forEach(function(node) {
					expect(node.index).toEqual(jasmine.any(Number));
				});
			});
			it("ensures that any solution nodes have an index property", function() {
				var nodes = graph.getNodes("solution");
				nodes.forEach(function(node) {
					expect(node.index).toEqual(jasmine.any(Number));
				});
			});
			it("has a resource node that references the tactical planner node", function() {
				var rNodes = graph.getNodes("resRoot"),
					pNodes = graph.getNodes("planRoot");
				expect(rNodes.length).toEqual(1);
				expect(pNodes.length).toEqual(1);
				expect(rNodes[0].parent).toEqual(pNodes[0].GUID);
			});
			it("has NO stored edges from mindmap nodes to drivers", function() {
				var edges = graph.edges(function(edge) {
					return (edge.source.visualType==="text" && edge.target.visualType==="driver");
				});
				expect(edges.length).toEqual(0);
			});
			it("has NO stored edges from mindmap nodes to features", function() {
				var edges = graph.edges(function(edge) {
					return (edge.source.visualType==="text" && edge.target.visualType==="feature");
				});
				expect(edges.length).toEqual(0);
			});
			it("has NO stored edges from mindmap nodes to solutions", function() {
				var edges = graph.edges(function(edge) {
					return (edge.source.visualType==="text" && edge.target.visualType==="solution");
				});
				expect(edges.length).toEqual(0);
			});
			describe("Driver Mindmap", function() {
				it("has four pre-defined subnodes",function() {
					var map1Root = graph.getNodes("map1Root")[0];
					expect(map1Root.outEdges.length).toEqual(4);
					var edges = map1Root.outEdges;
					edges.forEach(function(edge) {
						expect(edge.source).toEqual(map1Root);
//						expect(["array of titles"]).toContain(edge.target.title);
					});
				});
			});
//			it("has NO stored edges from segments to features", function() {
//				var edges = graph.edges(function(edge) {
//					return (edge.source.visualType==="segment" && edge.target.visualType==="feature");
//				});
//				expect(edges.length).toEqual(0);
//			});
//			it("has NO stored edges from segments to solutions", function() {
//				var edges = graph.edges(function(edge) {
//					return (edge.source.visualType==="segment" && edge.target.visualType==="solution");
//				});
//				expect(edges.length).toEqual(0);
//			});
		});

	});

});