define(['DataNode'],function(DataNode){
	'use strict';
	/*globals describe, it, expect, jasmine */
	/*jslint nomen:true */

	var da = [], nodes =[], vt="pinoccio";
	
	function populate() {
		var len=2, i, dat;
		for(i=0; i<len; i+=1) {
			dat = {GUID: "ID"+i, version:123, index:i, visualType:vt, y:i};
			da.push(dat);
			nodes.push(DataNode(dat)); //object constructor, so 'new' not needed
		}
	}
	populate();

	describe("DataNode", function(){
		it("rejects node data without GUID set", function() {
			expect(function() { new DataNode({}); }).toThrow("GUID must be defined");
			expect(function() { new DataNode({GUID:"123"}); }).not.toThrow();
		});
		it("is instantiated as expected", function() {
			var node = nodes[0];
			expect(node.toString()).toEqual("[object DataNode]");
		});
		it("methods operate as expected", function() {
			var node = nodes[0];
			expect(node.compareNodes(node)).toEqual(true);
			expect(node.isNodeVisible()).toEqual(false);
			node.visibleDegree = 2;
			expect(node.isNodeVisible()).toEqual(true);
		});
		it("defines edge arrays", function() {
			var node = nodes[0];
			expect(node.outEdges).toEqual(jasmine.any(Array));
			expect(node._outEdges).toEqual(jasmine.any(Array));
//			expect(node).toEqual(jasmine.any(DataNode));
		});
		it("allocates default values to undefined properties", function() {
			var node = nodes[0], props;
//			expect(node instanceof DataNode).toEqual(true);
			props = ["x","y","outDegree"];
			props.forEach(function(p) {
				expect(node[p]).toEqual(jasmine.any(Number));
			});
			props = ['outDegree','outEdges','_outEdges','visibleDegree',
				'inEdges','visible','parentTopics','visualType'];
			props.forEach(function(p) {
				expect(node[p]).toBeDefined();
			});
		});
		it("'inherits' from data as expected", function() {
			nodes.forEach(function(node, i) {
				expect(node.visualType).toBeDefined();
				expect(node.visualType).toEqual(da[i].visualType);
				expect(node.visualType).toEqual(vt);
				expect(node.y).toEqual(i);
				expect(node.index).toEqual(i);
			});
		});
		it("can read original parent data properties", function() {
			var newVT = "New Value";
			nodes.forEach(function(node, i) {
				expect(node.visualType).toEqual(vt);
				node.visualType = newVT;
				expect(node.visualType).toEqual(newVT);
				expect(node.getData("visualType")).toEqual(vt);
			});
		});
		it("can separately update parent data properties", function() {
			var newVT = "New Value";
			nodes.forEach(function(node, i) {
				expect(da[i].visualType).toEqual(vt);
				node.setData("visualType", newVT);
				expect(da[i].visualType).toEqual(newVT);
			});
		});
		it("will reflect changes in parent data if property isn't overridden", function() {
			var newVT = "New Value";
			nodes.forEach(function(node, i) {
				da[i].visualType = newVT;
				expect(node.visualType).toEqual(newVT);
			});
		});
		it("will ignore changes in parent data if property is overridden", function() {
			var newVT = "New Value", orVT = "Overridden Value";
			nodes.forEach(function(node, i) {
				node.visualType = orVT;
				da[i].visualType = newVT;
				expect(node.visualType).toEqual(orVT);
				node.setData("visualType", newVT);
				expect(node.visualType).toEqual(orVT);
			});
		});
	});

	describe("DataNodes", function(){
		it("instantiate as expected", function() {
			nodes.forEach(function(node, i){
				expect(node).toBeDefined();
				expect(node._outEdges).toEqual(jasmine.any(Array));
				expect(node.index).toEqual(i);
				expect(node.x).toEqual(jasmine.any(Number));
				node.x = i; //setup for next part of test
			});
			nodes.forEach(function(node, i){
				expect(node.x).toEqual(i);
			});
		});
		it("contains members from data as expected", function() {
			expect(nodes[0].version).toBeDefined();
			expect(nodes[0].version).toEqual(da[0].version);
			var props = ["x","y","outDegree"];
			props.forEach(function(p){
				expect(nodes[0][p]).toEqual(jasmine.any(Number));
			});
		});
	});

/*	describe("Inheritance", function() {
		it("recognizes members of the prototype object", function(){
			var q, p = {text:"from p", fn: function() { return 123; }};
			q = Object.create(p);
			q.x = 2;
			expect(q.text).toEqual("from p");
			expect(q.fn()).toEqual(123);
			expect(q.x).toEqual(2);
			q = {x:2}; q.__proto__ = p;
			expect(q.text).toEqual("from p");
			expect(q.fn()).toEqual(123);
			expect(q.x).toEqual(2);
		});
	});*/

/****
	describe("Minimal constructor", function() {
		it("sets default visualType", function() {
			var node = DataNode({GUID:"theGUID"});
			expect(node.visualType).toEqual("_default");
		});
	});

	describe("JavaScript inheritance", function() {
		it("should still have member functions", function() {
			function F() {
				this.testFn = function() { return "456"; };
			}
			F.prototype = {"testVal":"123"};
			var f = new F();
			expect(f.testFn).toBeDefined();
			expect(f.testFn()).toEqual("456");
			expect(f.testVal).toEqual("123");
			F.prototype.testFn = function() { return "789"; };
			expect(f.testFn()).toEqual("456");
			f = new F();
			expect(f.testFn()).toEqual("456");
		});
	});
***/
});

