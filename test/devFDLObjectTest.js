/**
Testing of both CoreGraph's data handling ability, and the testFullGraph structure's validity
for representing a full end-to-end usecase of the TRM-roadmap process.
*/
define(['VisualNode', 'VisualEdge', 'CoreGraph', 'd3'],
function(VisualNode, VisualEdge, CoreGraph) {
	'use strict';
	/*globals d3, it, describe, expect, dump */
	var graph = new CoreGraph();
	graph.addNode("nodeA", "Test", "");
	graph.addNode("nodeB", "Test", "map1Root", {"text":"the text"});
	graph.addNode("nodeC", "Test", "", {"text":"the text"});
	graph.addEdge("nodeA", "nodeB", "a-b", {"weight":1});
	graph.addEdge("nodeA", "nodeC", "a-c", {"weight":2});
	graph.addEdge("nodeB", "nodeA", "b-a", {"weight":3});
	graph.addEdge("nodeB", "nodeC", "b-c", {"weight":4});
	
	describe("VisualNode.convert", function() {
		var na = graph.nodes;
		it("starts with unaugmented array", function() {
			na.forEach(function(node) {
				expect(node.toString()).toEqual("[object DataNode]");
				expect(node.draw).toBeUndefined();
			});
		});
		it("augments all nodes in the given array", function() {
			var res = VisualNode.convert(na);
			expect(res.length).toEqual(na.length);
			res.forEach(function(node) {
				expect(node.toString()).toEqual("[object VisualNode]");
//				expect(node.isTagNode).toBeDefined();
				expect(node.isMapRootNode).toBeDefined();
				expect(node.draw).toBeDefined();
				expect(node.editText).toBeDefined();
				expect(node.getFill).toBeDefined();
			});
		});
	});
	
	describe("Augmented visualNodes", function() {
		var na = graph.nodes;//already augmented
		it("return appropriate display text values", function() {
			na.forEach(function(node) {
				var txt = node.getDisplayText();
				if(node.GUID==="nodeC") {
					expect(txt).toEqual("the text");
				} else {
					expect(txt).toEqual("Test");
				}
			});
		});
	});

	describe("VisualEdge.convert", function() {
		var ea = graph.edges();
		it("starts with unaugmented array", function() {
			ea.forEach(function(edge) {
				expect(edge.toString()).toEqual("[object DataEdge]");
			});
		});
		it("augments all nodes in the given array", function() {
			var res = VisualEdge.convert(ea);
			expect(res.length).toEqual(ea.length);
			res.forEach(function(edge) {
				expect(edge.toString()).toEqual("[object VisualEdge]");
			});
		});
	});
});

