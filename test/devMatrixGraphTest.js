define(['MatrixGraph'],function(MatrixGraph){
	'use strict';
	/*globals d3, it, describe, expect, dump, beforeEach */

	var graph;
	function initGraph() {
		graph = new MatrixGraph();
		graph.addNode("nodeA", "Test", "");
		graph.addNode("nodeB", "Test", "");
		graph.addNode("nodeC", "Test", "");
		graph.addEdge("nodeA", "nodeB", "a-b", {"weight":1});
		graph.addEdge("nodeA", "nodeC", "a-c", {"weight":2});
		graph.addEdge("nodeB", "nodeA", "b-a", {"weight":3});
		graph.addEdge("nodeB", "nodeC", "b-c", {"weight":4});
	}

	describe("MatrixGraph", function() {
		beforeEach(function() {
			initGraph();
		});
		it("can return filtered list of edges", function() {
			var edges = graph.getEdgesByFieldValue("relType", ["a-b"]);
			expect(edges.length).toEqual(1);
			edges = graph.getEdgesByFieldValue("sourceGUID", ["nodeA"]);
			expect(edges.length).toEqual(2);
			edges = graph.getEdgesByFieldValue("targetGUID", ["nodeB","nodeC"]);
			expect(edges.length).toEqual(3);
		});
		it("can create maps of edges", function() {
			var map;
			map = MatrixGraph.toSourceMap(graph.edges());
			expect(map.keys().length).toEqual(2);
			expect(map.get("nodeA").keys().length).toEqual(2);
			
			map = MatrixGraph.toTargetMap(graph.edges());
			expect(map.keys().length).toEqual(3);
			expect(map.get("nodeA").keys().length).toEqual(1);
		});
		it("can create default edge GUIDs", function() {
			var edges = graph.edges(), guid = MatrixGraph.defaultEdgeGUID(edges[0]);
			expect(guid).toBeDefined();
			expect(typeof guid).toEqual("string");
			expect(guid.length).toBeGreaterThan(1);
		});
		it("can fully connect between given edge arrays", function() {
			var edges = graph.fullyConnect(graph.nodes, graph.nodes, "new link"),
			nodeCount = graph.nodes.length;
			expect(edges.length).toEqual(nodeCount * nodeCount);
			graph.nodes.forEach(function(node) {
				expect(node.outEdges.length).toEqual(nodeCount);
				node.outEdges.forEach(function(edge) {
					var i = graph.nodes.indexOf(edge.target);
					expect(i).toBeGreaterThan(-1);
				});
			});
			edges.forEach(function(edge) {
				expect(edge.check()).toEqual(true);
			});
		});
	});
	describe("MatrixGraph.toMatrixObject", function() {
		var mx;
		beforeEach(function() {
			initGraph();
			mx = MatrixGraph.toMatrixObject(graph.edges());
			function sortByGUID(n1, n2) {
				return n1.GUID > n2.GUID ? 1 : -1;
			}
			mx.srcNodes.sort(sortByGUID);
			mx.tgtNodes.sort(sortByGUID);
		});
		it("can turn a set of edges into a matrix object", function() {
			expect(mx.grid()).toEqual([[0,3],[1,0],[2,4]]);
			expect(mx.colHeads(function(node) { return "x"+node.GUID;})).toEqual(["xnodeA","xnodeB"]);
			expect(mx.colHeads(function(n, i) { return i + n.title;})).toEqual(["0Test","1Test"]);
			expect(mx.rowHeads()).toEqual(["nodeA","nodeB","nodeC"]);
//				A	B
//			A	0	3
//			B	1	0
//			C	2	4
		});
		it("can return selected columns", function() {
			var col = mx.column(1);
			expect(col).toEqual([3,0,4]);
		});
		it("can create a matrix grid with custom values", function() {
			var grid = mx.grid(function() { return 7;});
			expect(grid).toEqual([[7,7],[7,7],[7,7]]);
			grid = mx.grid(function(e) { return (e) ? e.toString() : "n/a";});
			expect(grid[0]).toEqual(["n/a", "[object DataEdge]"]);
		});
		it("can create a transpose grid", function() {
			var grid = mx.transpose();
			expect(grid).toEqual([[0,1,2],[3,0,4]]);
//				A	B	C
//			A	0	1	2
//			B	3	0	4
			//doesn't transpose row/col heads though
			expect(mx.rowHeads()).toEqual(["nodeA","nodeB","nodeC"]);
		});
	});

});
