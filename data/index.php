<html>
	<body>
		<h3>Stored roadmaps.</h3>
		<ul>
<?php 
$files = scandir(".");
$arrlength=count($files);

for($x=0;$x<$arrlength;$x++) {
  $file = $files[$x];
  $point = stripos($file, ".");
  $prefix = substr($file, 0, $point);
  $suffix = substr($file, $point);
  if(strcasecmp($suffix, ".json") == 0) {
		echo "<li>$prefix</li>";
  }
}
?>
		</ul>
	</body>
	
</html>
