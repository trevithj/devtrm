<?php 
if(isset($_REQUEST['data'])) {
	$File = "saved.json";
	if(isset($_REQUEST['file'])) {
		$File = $_REQUEST['file'];
	}
	$handle = fopen($File, 'w');
	if(!$handle) {
		print "{\"status\":\"ERR\",\"msg\":\"Cannot open file $File\"}"; 
		exit;
	}
	$data = $_REQUEST['data'];
	if (fwrite($handle, $data) === FALSE) {
		print "{\"status\":\"ERR\",\"msg\":\"Cannot write to file $File\"}"; 
		exit;
	}
	fclose($handle); 
	print "{\"status\":\"OK\",\"msg\":\"Data written to $File\"}"; 
} else {
	$Str = implode(",", $_REQUEST);
	print "{\"status\":\"ERR\",\"msg\":\"No data passed:\",\"vals\":[$Str]}"; 
//	print "{\"status\":\"ERR\",\"msg\":\"No data passed:\"}"; 
}
?>