define(['mohio', 'src/view/PlannerSolutions', 
	'src/view/PlannerTasks', 'src/view/PlannerResources',
	'src/view/PlannerResAlloc', 'd3'],
 function (mohio, addSolutions, addTasks, addResources, addAllocs) {
	'use strict';
	/*globals d3:false  */
	var append = function (trm) {
		trm.planner = trm.planner || {};
		trm = addSolutions(trm);
		trm = addTasks(trm);
		trm = addResources(trm);
		trm = addAllocs(trm);

		var divID = trm.divIDs[8];
		
		function highlightRow(el, tbl) {
			var tr = el.node().parentNode;
			tbl.select("tbody").selectAll("tr")
			.classed("selected-row", false);
			d3.select(tr).classed("selected-row", true);
		}
		function updateCell(el, obj, msg) {
			var val = mohio.core.prompt(msg, obj.get());
			if(val !== null) {
				obj.set(val);
				el.text(val);
			}
		}

		trm.planner.show = function () {
			var tbl, tr, div = mohio.core.clearChildren(divID);
			div.append("h3").text("Planner: solutions to resourced tasks");
			tbl = div.append("table").attr("class", "planner");
			tr = tbl.append("tr");
			trm.planner.solutions.addTable(tr.append("td"), divID);
			trm.planner.tasks.addTable(tr.append("td"), divID);
			tr = tbl.append("tr");
			trm.planner.resources.addTable(tr.append("td"), divID);
			trm.planner.allocs.addTable(tr.append("td"), divID);
		};

		trm.planner.highlightRow = highlightRow;
		trm.planner.updateCell = updateCell;
		return trm;
	};

	return append;
});
