define(['mohio', 'src/view/Segments', 'src/view/Matrix1', 'src/view/Matrix2', 'src/view/Matrix3', 'd3'],
 function (mohio, addSegments, addMatrix1, addMatrix2, addMatrix3) {
	'use strict';
	/*globals d3:false  */
	var append = function (trm) {
		trm.matrix = trm.matrix || {};
		trm = addSegments(trm);
		trm = addMatrix1(trm);
		trm = addMatrix2(trm);
		trm = addMatrix3(trm);


		function reverseSort(a, b) {
			return (a.title > b.title) ? -1 : 1;
		}
		function forwardSort(a, b) {
			return (a.title < b.title) ? -1 : 1;
		}
		trm.matrix.sortNodes = function(nodes, reverse) {
			if(reverse) {
				return nodes.sort(reverseSort);
			} //else
			return nodes.sort(forwardSort);
		};


		trm.matrix.getNodeEdges = function(srcVT, tgtVT, defRelType, defVType) {
			defRelType = defRelType || srcVT + "- " + tgtVT;
			defVType = defVType || "cell";
			var srcNodes = trm.graph.getNodes(srcVT),
				tgtNodes = trm.graph.getNodes(tgtVT),
				defProps = {"weight":0, visualType: defVType},
				allEdges = trm.graph.fullyConnect(srcNodes, tgtNodes, defRelType, defProps);
			return {srcNodes:srcNodes, tgtNodes:tgtNodes, edges:allEdges};
		};


		trm.matrix.writeColumnHeaders = function(tbl, headNodes, headNodes2) {
			var heads = headNodes(function(node) { return (node) ? node.title : "?";}),
				heads2 = (headNodes2) ?
				headNodes2(function(node) { return (node) ? node.title : "?";}) : [];
			heads = heads.concat(heads2);
			heads.unshift("-"); //add leading blank to cols
			mohio.table.populateTableHead(tbl, heads);
		};

		function updateWeightCell(td, msg, edge) {
			var newWgt = mohio.core.prompt(msg, edge.weight);
			if(newWgt !== null) {
				d3.select(td).text(newWgt);
				edge.setData("weight", newWgt);
			}
		}
		trm.matrix.updateWeightCell = updateWeightCell;


		function getEdgeOrThrow(edge) {
			if(edge) { return edge;}
			throw new Error("Edge is undefined");
		}

		function defaultSetClick(td) {
			var doDblClick = function(d) { updateWeightCell(this, "Enter new weight", d); };
			td.on("dblclick", doDblClick);
//			td.on("click", highlightRow??);
		}

		function getNodeTitle(node) { return (node) ? node.title : "??"; }

		trm.matrix.writeGrid = function(mx, tbl, toStringFn, setClickFn, noHead) {
			if(!noHead) {
				trm.matrix.writeColumnHeaders(tbl, mx.colHeads);
			}
			var heads = mx.rowHeads(getNodeTitle),
			tbody = tbl.append("tbody"),
			grid = mx.grid(getEdgeOrThrow);
			toStringFn = toStringFn || function(e) { return e.getData("weight");};
			setClickFn = setClickFn || defaultSetClick;
//			console.log(trm.matrix.gridToString(grid));
			grid.forEach(function (row, r) {
				var tr = tbody.append("tr");
				tr.append("th").text(heads[r]); //add row heading to grid rows
				mohio.table.addCellsToRow(tr, row,
					toStringFn,
					setClickFn
				);
			});
		};

		trm.matrix.gridToString = function(grid) {
			var txt = [];
			grid.forEach(function(row) {
				row.forEach(function(cell) {
					txt.push(cell.weight || cell.toString());
					txt.push("; ");
				});
				txt.push("\n");
			});
			return txt.join("");
		};


		function checkEdge(e) {
			var flag1 = (e.source.visualType==="segment")? "S": "",
				flag2 = (e.target.visualType==="segment")? "T" : "";
			return flag1 + flag2;
		}
		trm.matrix.checkEdge = checkEdge;


		trm.matrix.getCellColour = function(d) {
			var colr, flag = checkEdge(d);
			if(flag === "") {
				colr = "white";
			} else if(flag ==="S") {
				colr = trm.getColour(d.source.index);
			} else if(flag ==="T") {
				colr = trm.getColour(d.target.index);
			} else {
				colr = trm.getColour(Math.min(d.source.index, d.target.index));
			}
			return colr;
		};


		return trm;
	};

	return append;
});
