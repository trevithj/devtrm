define(["MatrixGraph", "src/view/TabPanel",
	"src/view/ControlPanel", "d3"],
	function (MatrixGraph, addTabPanels, addControlPanel) {
	'use strict';
	/*globals d3 */
	function App(mohio) {
		var trm = {
			divIDs : [
				"divHome",
				"divDriversMap", "divSegments", "divMatrix1",
				"divFeaturesMap", "divMatrix2",
				"divSolutionsMap", "divMatrix3",
				"divPlanner", "divTRM"
			],
			divMapPanels : [1,4,6],
			divMatrixDivs : [2,3,5,7],
			divSpecial : ["divControl", "divTabs", "divPopup"],
			tabDivs : null,
			rootNodesMap: d3.map()
		},
		dataUrl = '../data/read.php',
		saveUrl = '../data/write.php',
		colourScale = d3.scale.category20(),
		colourScale2 = d3.scale.category10(),
		rawData = null; //helpful for testing in console
		trm.getRaw = function() {
			return rawData; //see also doLoad()
		};

		function getColour(index, scale,defaultCol) {
			index = (index===-1) ? "" : index;
			return (isNaN(index)) ? defaultCol : scale(index);
		}
		trm.getColour = function(index) {
			return getColour(index, colourScale, "silver");
		};
		trm.getResColour = function(index) {
			return getColour(index, colourScale2, "aqua");
		};
		trm.graph = new MatrixGraph();
		trm = addTabPanels(trm);
		trm = addControlPanel(trm);

		function mapRootNodes(rootID) {
			var rootNodes = trm.graph.getNodes(function(node) {
				return (node.parent === undefined) ?  false : (node.parent === rootID);
			});
			rootNodes.forEach(function(node) {
				var key = node.name;
				trm.rootNodesMap.set(key, node);
			});
		}


		function doLoad(filename, server) {
			server = server || dataUrl;
			server += "?ts="+new Date().getTime();
			server += (filename) ? "&file=" + filename : "";
			mohio.core.load(server, function (result) {
				var json = result.data;
				rawData = result.data; //for testing...
				trm.graph.newGraph(json, json.rootID);
				mapRootNodes(trm.graph.root.GUID);
				mohio.fire("graphLoaded", result, trm);
			});
		}

		function getEncodedGraph() {
			var dat = JSON.stringify(trm.graph.getRawGraph());
			//dat = dat.replace(/&/g,"%26");
			return encodeURIComponent(dat);
		}

		function doSave(filename) {
			filename = filename || "saved.json";
			var raw, xhr = d3.json(saveUrl);
			raw = "file="+filename+"&data=";
			raw += getEncodedGraph();
			xhr
			.header("Content-Type","application/x-www-form-urlencoded")
			.post(raw, function(err, result) {
				if(err) {
					mohio.fire("graphSaveError", err, trm);
				} else {
					mohio.fire("graphSaved", result, trm);
				}
			});
		}

		function trm_home_show() {//temp??
			var div = mohio.core.clearChildren(trm.divIDs[0]);
			d3.html("./Home.html", function(err, snippet) {
				if(err) {
					console.log(err);
				}
				div.node().appendChild(snippet);
//				var rawGraph = trm.getRaw();
//				div.append("pre").text(JSON.stringify(rawGraph, null, 3));
			});
		}

		function refreshDisplayedDiv(id) {
			if(id==="divHome") {
				trm_home_show();
			} else if(id==="divSegments") {
				trm.segments.show(id);
			} else if(id==="divMatrix1") {
				trm.matrix1.show(id);
			} else if(id==="divMatrix2") {
				trm.matrix2.show(id);
			} else if(id==="divMatrix3") {
				trm.matrix3.show(id);
			} else if(id==="divDriversMap") {
				trm.mindMap1.show();
			} else if(id==="divFeaturesMap") {
				trm.mindMap2.show();
			} else if(id==="divSolutionsMap") {
				trm.mindMap3.show();
			} else if(id==="divPlanner") {
				trm.planner.show();
			} else if(id==="divTRM") {
				trm.roadMap.show();
			}//else ignore
		}

		function doInit() {
//			d3.select("body").selectAll("div").remove();//clear divs
//			var parent = d3.select("body").insert("div").attr("id", "mohio-main");
			var parent = mohio.core.clearChildren("mohio-main"); //if div is pre-defined
			mohio.core.makeDivs(trm.divSpecial, parent);
			trm.tabDivs = mohio.core.makeDivs(trm.divIDs, parent);
			trm.tabDivs.attr("class", "tabbedPane");
			document.getElementById('nav-bar').appendChild(document.getElementById('divControl'));
			trm.control.show("divControl");
			document.getElementById('nav-bar').appendChild(document.getElementById('divTabs'));
			trm.tabPanel.show("divTabs");//Hard-coded??

			mohio.bind("tabDisplayed", refreshDisplayedDiv);
			mohio.bind("clickedNew", function() {
				doLoad();
			});
			mohio.bind("clickedLoad", function() {
				var filename = mohio.core.prompt(
					"Please enter a filename to open",
					trm.control.lastLoadedFileName
				);
				if(filename) {
					trm.control.lastLoadedFileName = filename;
					doLoad(filename + ".json");
				} //else ignore
			});
			mohio.bind("clickedSave", function() {
				var filename = mohio.core.prompt(
					"Please enter a filename for your roadmap",
					trm.control.lastSavedFileName
				);
				//NOTE: no suffix, that is added below
				if(filename) {
					trm.control.lastSavedFileName = filename;
					doSave(filename + ".json");
				} //else ignore
			});
			mohio.bind("graphLoaded", function() {
				trm.hideOtherDivs("divHome");
				trm_home_show();
			});
			mohio.bind("graphSaved", function(result) {
				console.log(["122", result.status, result.msg]);
			});
		}

		trm.init = doInit;
		trm.load = doLoad;
		trm.save = doSave;
		mohio.trm = trm;
		return mohio;
	}

	return App;
});
