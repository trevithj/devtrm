define(['mohio'], function (mohio) {
	'use strict';
	function Traverse(roadMap) {
		var ret = {};

		function moveSrcTaskBackward(src, tgt) {
			var end = roadMap.calcEnd(src.start, src.duration),
				start = mohio.core.toNum(tgt.start),
				overlap = end - start;
			if(overlap > 0) {
				start = mohio.core.toNum(src.start) - overlap;
				src.setData("start", Math.max(start, 0));
			}
		}

		function traverseBackward(task, level) {
			level = level || 0;
			if (level > 50) {
				console.log("traverseBackward at 50 levels ??");
				return; //something wrong here!
			}
			task.inEdges.forEach(function(edge) {
				if(edge.source.visualType==="task") {
					moveSrcTaskBackward(edge.source, edge.target);
					if(edge.source.start>0) {
						traverseBackward(edge.source, level+1);
					}
				}
			});
		}



		function moveTgtTaskForward(src, tgt) {
			var end = roadMap.calcEnd(src.start, src.duration);
			tgt.setData("start", Math.max(tgt.start, end));
		}


		function traverseForward(task, level) {
			level = level || 0;
			if (level > 50) {
				console.log("traverseForward at 50 levels ??");
				return; //something wrong here!
			}
			if(task.start<0) {
				task.setData("start", 0);
			}
			task.outEdges.forEach(function(edge) {
				if(edge.target.visualType==="task") {
					moveTgtTaskForward(edge.source, edge.target);
					traverseForward(edge.target, level+1);
				}
			});
		}

		ret.backward = traverseBackward;
		ret.forward = traverseForward;

		return ret;
	}

	return Traverse;
});