define(['mohio', 'Panel', 'src/view/RoadMapTasks', 'src/view/RoadMapLoads', 'd3'],
 function (mohio, Panel, addTasks, addLoads) {
	'use strict';
	/*globals d3:false  */
	var append = function (trm) {
		trm.roadMap = trm.roadMap || {};
		var divID = trm.divIDs[9],
			taskPanelID = divID+"-tasks",
			loadPanelID = divID+"-loads",
			leftMargin = 200;
		trm.roadMap.taskPanel = null;
		trm.roadMap.loadPanel = null;
		trm = addTasks(trm);
		trm = addLoads(trm);

		function setMargin(panel) {
			panel.resize();
			panel.setHorizontalMargins(0, leftMargin);
			panel.theVis().attr("transform", "translate("+leftMargin+",0)");
			var margin, yRange = panel.yScale().range();
//				marginLine = {x1:0, y1:yRange[0], x2:0, y2:yRange[1]};
			margin = panel.theVis().selectAll("line.margin").data([1]);
			margin.enter().append("line").attr("class", "margin")
			.attr("x1", 0)
			.attr("y1", yRange[0])
			.attr("x2", 0)
			.attr("y2", yRange[1])
			.attr("stroke", "silver");
			margin.exit().remove();
		}
		
		function setTimeAxis(panel) {
			var g, h, axis = d3.svg.axis()
			.scale(panel.xScale());
			h = panel.yScale().range()[1] - 24;
			g = panel.theVis().selectAll("g.axis").data([1]);
			g.enter().append("g").attr("class", "axis");
			g.attr("transform", "translate(0,"+h+")");
			g.call(axis);
		}
		
		function initDisplay() {
			if(trm.roadMap.taskPanel === null) {
				var divs, div = mohio.core.clearChildren(divID);
				div.append("h3").text("Technology Roadmap");
				divs = mohio.core.makeDivs([taskPanelID, loadPanelID], div);
				trm.roadMap.taskPanel = new Panel(taskPanelID);
				trm.roadMap.loadPanel = new Panel(loadPanelID);
				trm.roadMap.loadPanel.theDiv(true).insert("h3", "svg").text("Resource Loading");
				trm.roadMap.taskPanel.xScale().domain([0,20]);
				trm.roadMap.loadPanel.xScale().domain([0,20]);
//				trm.roadMap.timeGrid = new TimeGrid(trm.roadMap.taskPanel);
			}
			setMargin(trm.roadMap.taskPanel);
			setMargin(trm.roadMap.loadPanel);
			setTimeAxis(trm.roadMap.taskPanel);
//			trm.roadMap.timeGrid.refresh();
		}
		

		trm.roadMap.show = function() {
			initDisplay();
			trm.roadMap.showTasks();
			trm.roadMap.showLoads();
		};
		mohio.bind("taskBlocksChanged", trm.roadMap.show );

		return trm;
	};
	
	return append;
});