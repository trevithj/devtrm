define(['mohio', 'VisualNode', 'VisualEdge', 'Panel',
	'src/view/Map1', 'src/view/Map2', 'src/view/Map3', 'd3'],
function(mohio, VisualNode, VisualEdge, Panel, addMap1, addMap2, addMap3) {
	'use strict';
	/*globals d3:false  */
	var append = function(trm) {
		trm.mindMap = trm.mindMap || {};
		trm.mindMap.selected = null;
		trm.mindMap.views = d3.map();
		trm.mindMap.edges = null;
		trm.mindMap.panel = null;
		trm = addMap1(trm);
		trm = addMap2(trm);
		trm = addMap3(trm);
		trm.mindMap.maps = [trm.mindMap1, trm.mindMap2, trm.mindMap3];

		function moveNode(g, node) {
			VisualNode.move(g, node.x, node.y);
			VisualEdge.draw(trm.mindMap.panel, trm.mindMap.edges);
		}

//		function getMouseXY(g) {
//			var parent = g.parentNode;
//			return d3.mouse(parent);
//		}
		function setNodeDomainXY(node, x,y) {
			node.setData("domainX", d3.round(x,2));
			node.setData("domainY", d3.round(y,2));
		}

		trm.mindMap.drag = d3.behavior.drag()
		.on("drag", function(d) {
			d.x += d3.event.dx;
			d.y += d3.event.dy;
			moveNode(d3.select(this), d);
		})
		.on("dragend", function(d) {
			var dmnX, dmnY;
			dmnX = trm.mindMap.panel.xScale().invert(d.x);
			dmnY = trm.mindMap.panel.yScale().invert(d.y);
			setNodeDomainXY(d, dmnX, dmnY);
		});

		trm.mindMap.getMapNodes = function(mapVType) { //eg, map1Root
			var nodes = {}, all = [], guids = [];
			function visitTextNodes(n) {
				var nVType = n.visualType, ret = "alreadyVisited";
				if(nVType !== "text" && nVType !== mapVType) {
					console.log(n.GUID, nVType); //?? why even visited?
					ret = "noVisit";
				} else if(guids.indexOf(n.GUID) === -1) {
					n = VisualNode.augment(n); //augment node here...
					guids.push(n.GUID);
					all.push(n);
					ret = "visitOK";
				} //else
				return ret;
			}
			nodes.mapRoot = trm.graph.getNodes(mapVType)[0]; //assume only 1
			if(!nodes.mapRoot) {
				throw new Error("Can't find mapNode with visualType="+mapVType);
			}
			nodes.mapRoot.dfs(visitTextNodes);
			nodes.all = all; //nodes.drivers.concat(nodes.dText);
			return nodes;
		};

		trm.mindMap.getMapEdges = function(relTypes) {
			var edges = trm.graph.edges(function(edge) {
				return( relTypes.indexOf(edge.relType) > -1);
			});
			VisualEdge.convert(edges);//augment each edge
			return edges;
		};

		function setNodeXY(panel, node, x, y) {
			x = (x!==undefined) ? x : Number(node.getData("domainX"));
			y = (y!==undefined) ? y : Number(node.getData("domainY"));
			node.x = panel.xScale(x);
			node.y = panel.yScale(y);
			node.x = d3.round(node.x);
			node.y = d3.round(node.y);
			setNodeDomainXY(node, x,y);
		}
		function layoutNodes(panel, nodes) {
			panel.resize();
			var x=50, y=-60;
			nodes.forEach(function(node) {
				if(node.domainX!==undefined && node.domainY!==undefined) {
					setNodeXY(panel, node);
				} else if(node.isMapRootNode()) {
					setNodeXY(panel, node, 0, 0);
				} else {
					setNodeXY(panel, node, x, y);
					x= x*-1; y+= 20;
				}
			});
		}

		function addChildNode(srcNode, relType, panel) {
			var node, tgtGuid = "MMap"+new Date().getTime();
			node = trm.graph.addNode(tgtGuid, "text", "text",
				{"text": "Double-click to edit"}
			);
			node.x = panel.xScale().invert(srcNode.x);
			node.y = panel.yScale().invert(srcNode.y);
			setNodeDomainXY(node, node.x, node.y - 20);
			trm.graph.addEdge(srcNode.GUID, tgtGuid, relType,
				{"visualType":"text"}
			);
		}

		trm.mindMap.addChildNode = addChildNode;


		function handleNodeClick(d) {
			if(trm.mindMap.selected) {
				trm.mindMap.selected.node.selected = false;
			}
			trm.mindMap.selected = null;
			d.selected = true;
			if(!d.isMapRootNode()) {
				trm.mindMap.selected = {"node": d, "scope": trm.mindMap};
			}
		}

		trm.mindMap.handleNodeClick = handleNodeClick;


		function showMap(panel, nodes, edges) {
			var gNodes;
			layoutNodes(panel, nodes);
			VisualEdge.draw(panel, edges);

			gNodes = panel.theVis().selectAll("g.node").data(nodes);
			gNodes.enter().append("g").attr("class", "node")
			.append("title").text("Shift-click to add new child node"); //tooltip
			gNodes.exit().remove();
			VisualNode.draw(gNodes);
			gNodes.on("dblclick", function(d) {
				d.editText(d3.select(this));
			});
			return gNodes; //allow chaining
		}
		trm.mindMap.showMap = showMap;


		function makeMapView(id) {
			var view = {},
				mapPanelID = id+"-panel",
				tagTableID = id+"-table",
				parent = mohio.core.clearChildren(id);
			if(parent.empty()) {throw "Parent is empty";}
			view.panelDiv = parent.append("div")
			.attr("id", mapPanelID).attr("class", "mapPanelDiv");
			view.tableDiv = parent.append("div")
			.attr("id", tagTableID).attr("class", "tagTableDiv");
			view.panel = new Panel(mapPanelID);
			view.panel.xScale().domain([-100, +100]);
			view.panel.yScale().domain([-100, +100]);
			view.tableDiv.append("h3").text("pending");
			view.tableDiv.append("button").text("Add");
			view.table = mohio.table.make(view.tableDiv, "tagTable");
			mohio.table.populateTableHead(view.table, ["Title","Index"]);
			return view;
		}

		trm.mindMap.makeViews = function(viewDivIDs) {
			viewDivIDs.forEach(function(i, n) {
				var id = trm.divIDs[i],
				view = makeMapView(id);
				trm.mindMap.views.set(id, view);
				trm.mindMap.maps[n].view = view;
			});
		};


		function tagTextNode(tr) {
			if(trm.mindMap.selected) {
				var node = trm.mindMap.selected.node,
					index = d3.select(tr).select("td.col1").text();
				index = parseInt(index, 10);
				node.setData("index", index);
				node.draw();
			}
		}

		trm.mindMap.refreshTable = function(view, heading, vType, dblClickFn, clickFn) {
			clickFn = clickFn || tagTextNode;
			var nodes;//, view = trm.mindMap.views.get(divID);
			view.tableDiv.select("h3").text(heading);
			nodes = trm.graph.getNodes(vType);
			mohio.table.populateTableBody(view.table, nodes, function(node) {
				return [node.title, node.index];
			});
			view.table.select("tbody")
				.selectAll("tr")
				.style("background-color", function(node) {
					return trm.getColour(node.index);
				})
				.on("dblclick", function() {
					dblClickFn(this);
				})
				.on("click", function() {
					clickFn(this);
				});
			return view; //for chaining
		};


		return trm;
	};

	return append;
});
