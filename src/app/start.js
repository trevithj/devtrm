







require.config({
	baseUrl: '..',
	paths: {
		'jquery': 'lib/jquery-1.7.1',
		'mohio': 'src/base/Mohio',
		'coreMohio': 'src/base/Mohio.core',
		'DataEdge': 'src/model/DataEdge',
		'DataNode': 'src/model/DataNode',
		'VisualEdge': 'src/view/VisualEdge',
		'VisualNode': 'src/view/VisualNode',
		'CoreGraph': 'src/model/Graph.core',
		'MatrixGraph': 'src/model/Graph.matrix',
		'Panel': 'src/view/Panel',
		'theApp': 'src/app/App',
		'd3': 'lib/d3'
	}
});

/** dependencies moved to the module code with define(['jquery', ;d3']...*/
require(['theApp', 'mohio'],

function (makeApp, mohio) {
	'use strict';
	mohio = makeApp(mohio);
	mohio.trm.init();
	var autoLoadFileName = mohio.trm.control.lastLoadedFileName;
	autoLoadFileName = (autoLoadFileName==="saved") ? null : autoLoadFileName+".json";
	mohio.trm.load(autoLoadFileName);
});
