define(['d3'], function () {
	'use strict';
	/*globals d3:false  */
	var append = function (mohio) {
		mohio.table = mohio.table || {};

		function make(div, className) {
			return div.append("table").attr("class", className);
		}

		function populateTableHead(tbl, vals) {
			tbl.selectAll("thead").remove();
			tbl.append("thead").append("tr").selectAll("th")
			.data(vals)
			.enter().append("th").text(function (d) {
				return d;
			});
		}

		function populateTableBody(tbl, rowData, cellDatFn) {
			cellDatFn = cellDatFn || function (d) {
				return d;
			};
			tbl.selectAll("tbody").remove();
			var tr = tbl.append("tbody").selectAll("tr")
				.data(rowData)
				.enter().append("tr");
			/*jslint unparam:true */
			tr.selectAll("td").data(cellDatFn)
			.enter().append("td")
			.text(function (d) { return d; })
			.attr("class", function(d, i) {
				return "col"+i;
			});
		}

		function addCellsToRow(trSel, data, toStringFn, setClickFn, cellClassFn) {
			toStringFn = toStringFn || function(d) { return d; };
			cellClassFn = cellClassFn || function(edge) { return edge.target.GUID;};
			var td = trSel.selectAll("td").data(data);
			td.enter()
			.append("td")
			.text(toStringFn)
			.attr("class", cellClassFn);
			if(setClickFn && typeof setClickFn === "function") {
				setClickFn(td);
			}//else ignore
		}

		mohio.table.make = make;
		mohio.table.populateTableHead = populateTableHead;
		mohio.table.populateTableBody = populateTableBody;
		mohio.table.addCellsToRow = addCellsToRow;


		mohio.table.makeEntryForm = function(divID, fieldArray, options) {
			options = options || {}; //TODO
			var html, div = d3.select("#"+divID);
			if(div.empty()) {
				return null;
			}
			html = ["<form><table class='entryFormTable'><tbody>"];
			fieldArray.forEach(function(fName) {
				html.push("<tr><th>");
				html.push(fName);
				html.push("</th><td>");
				html.push("<input type='text' name='");
				html.push(fName);
				html.push("'/></td></tr>");
			});
			html.push("</tbody></table></form>");
			div.html(html.join(""));
			return div;
		};

		mohio.table.parseEntryForm = function(divID) {
			var result = {},
			fm = d3.select("#"+divID).select("form");
			fm.selectAll("input").each(function() {
				result[this.name] = this.value;
			});
			return result;
		};

		mohio.table.showEdges = function(edges, divID, name) {
			var tbl,
				div = mohio.core.clearChildren(divID);
			name = name || divID;
			tbl = make(div, "matrix");
			populateTableHead(tbl, ["GUID", "relType", "visType", "sourceGUID", "targetGUID", "Wgt"]);
			populateTableBody(tbl, edges, function (edge) {
				return [edge.GUID, edge.relType, edge.visualType,
					edge.sourceGUID, edge.targetGUID, edge.getData("weight")];
				//MatrixGraph.getEdgeWeight(edge)];
			});
		};


		return mohio;
	};

	return append;
});

