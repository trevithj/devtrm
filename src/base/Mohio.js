/** Build the library here...*/
define(["coreMohio", "src/base/Mohio.table"], function (addCore, addTable) { //, addDraw, addShapes, addSerialiser, addBehaviours) {
	'use strict';

	// First, check that this version of javascript does what we want.
	if (!Array.prototype.forEach) {
		Array.prototype.forEach = function (fn, scope) {
			var i, len;
			for (i = 0, len = this.length; i < len; i += 1) {
				fn.call(scope, this[i], i, this);
			}
		};
	}

	if (typeof Object.create !== 'function') {
		Object.create = function (o) {
			function F() {}
			F.prototype = o;
			return new F();
		};
	}



	var mohio = {},
	 events = [];

	mohio.debugOn = false;
	mohio.log = function (m) {
		if (this.debugOn) {
			var msg = arguments.length > 1 ? [arguments] : m;
			console.log(msg); //TODO: generalize this?
			if(msg.stack) {
				console.log(msg.stack);
			}
			//perhaps use log array and push messages instead?
			//Display them all at once if requested from console?
		}
	};


	mohio.clone = function(obj){ //only copies hasOwnProperties
		if(obj && typeof obj === 'object') {
			var key, temp = obj.constructor(); // changed
			for(key in obj) {
				if(obj.hasOwnProperty(key)) {
					temp[key] = obj[key];
				}
			}
			return temp;
		} //else
		return null;
	};


	/**
	 * Uses an array of arrays to map an event name to one or more functions.
	 * @param {type} defaultScope use when firing an event, if scope not defined.
	 * @returns {_L1.Observer}
	 */
	function bind(eventName, method) {
		if (typeof method === "function") {
			if (!events[eventName]) {
				events[eventName] = [];
				mohio.log(eventName + " added");
			}
			events[eventName].push(method);
		} else {
			mohio.log(eventName + " NOT added");
		}
	}

	/**
	* Params expectes to be an array, but will take any object. It is up to
	* the receiving function to know the difference. Usage can be either:
		bind("sumXYZ", function(x,y,z) { return x+y+z; });
		fire("sumXYZ", ["x","y","z"] );
	* or:
		bind("sumXYZ", function(p) { return p.x + p.y + pz; });
		fire("sumXYZ", {"x":1,"y":2,"z":9} );
	*/
	function fire(eventName, params, scope) {
		scope = scope || window;
		if (params && !(params instanceof Array)) {
			params = [params]; //wrap in array
		}
		if (events[eventName]) {
			mohio.log("Firing " + eventName);
			events[eventName].forEach(function (event) { //,index, events
//				event.apply(scope, params);
				setTimeout(function() {
					event.apply(scope, params);
				}, 0); //so an error in one method doesn't stop the others (test?)
			});
		} else { //nothing to fire
			mohio.log(eventName + " noted");
		}
	}

	mohio.bind = bind;
	mohio.fire = fire;

	mohio = addCore(mohio);
	mohio = addTable(mohio);
//	mohio = addDraw(mohio); //also handles drawNodes, drawLinks, draw.update
//	mohio = addShapes(mohio);
//	mohio = addSerialiser(mohio);
//	mohio = addBehaviours(mohio);

	// Can bind functions here if we want.
//	mohio.bind("getNewGraph", function () {
//		mohio.log(arguments);
//	});
	mohio.globalScopeCheck = function() {
		var p, pa = [];
		for( var p in window ) {
			if(window.hasOwnProperty(p)) {
				pa.push(p);
			}
		}
		pa.sort();
		console.log(pa);
		return pa;
	};

	window.mohio = mohio; //maybe leave this for caller?
	return mohio;
});
