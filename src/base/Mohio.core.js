define(['d3'], function () {
	'use strict';
	/*globals d3:false  */
	var append = function (mohio) {
		mohio.core = mohio.core || {};
		/**
		 * Retreives graph data from the server and passes to a custom function that
		 * will handle the data.
		 * @param {String} server The hostname for the request.
		 * @param {String} rootGUID The GUID of the new root node requested.
		 * @param {String} pipelineID
		 * @param {Function} callback Function to be called once request is completed,
		 * taking a single object parameter, where:
		 *  obj.data = the graph data object.
		 *  obj.error (if defined) = a d3 error indicating unsuccessful load.
		 * @returns {String} url for debugging.
		 */
		mohio.core.loadGraph = function (server, rootGUID, pipelineID, callback) {
			var url = server + "?method=doRead&command=get_node_data&GUID=" +
				rootGUID + "&pipelineID=" + pipelineID;
			return mohio.core.load(url, callback);
		};

		/**
		 * Retreives resource data from the given url and passes to a custom function that
		 * will handle the data.
		 * @param {String} url The location of the resource.
		 * @param {Function} callback Function to be called once request is completed,
		 * taking a single object parameter, where:
		 *  obj.data = the graph data object.
		 *  obj.error (if defined) = a d3 error indicating unsuccessful load.
		 * Note: can also pass a string, which will be treated as an event name. See fire().
		 * @returns {String} url for debugging.
		 */
		mohio.core.load = function (url, callback) {
			d3.json(url, function (err, data) {
				var result = {};
				result.data = data;
				result.error = err;
				if (typeof callback === 'function') {
					callback.call(mohio, result);
				} else if (typeof callback === 'string') {
					mohio.fire(callback, result, mohio);
				}
				return result;
			});
			return url;
		};



		/**
		 *Add a default edge relevance value if not already defined
		 * @param {Graph} graph
		 * @returns {Graph}
		 */
		mohio.core.setDefaultEdgeRelevance = function (graph) {
			var i,
			len = graph.edges.length,
			rel,
			lnk;
			for (i = 0; i < len; i+=1) {
				rel = (1 + i) / len; //based on order of links
				lnk = graph.edges[i];
				lnk.relevance = lnk.relevance || rel;
				//	self.graph.links[i].title += " "+self.graph.links[i].relevance;
			}
			return graph;
		};

		mohio.core.setAllVisible = function (graph) {
			var i,
			len = graph.nodes.length;
			for (i = 0; i < len; i+=1) {
				graph.nodes[i].visible = true;
			}
			len = graph.edges.length;
			for (i = 0; i < len; i+=1) {
				graph.edges[i].visible = true;
			}
		};

		mohio.core.addChildIfNotThereAlready = function(d3Parent, type) {
			var ret = d3Parent.select(type);
			ret = (ret.empty()) ? d3Parent.append(type) : ret;
			return ret;
		};


		/**Breaks a line of text into multiple lines (if needed)
		 * @param {String} txt
		 * @param {int} charsPerLine
		 * @param {int} maxDepth
		 * @returns {Array} an array containing the lines of text
		 */
		mohio.core.formatString = function (txt, charsPerLine, maxDepth) {
			var result = [],
			i;
			while (maxDepth > 0) {
				if (txt.length <= charsPerLine) {
					result.push(txt);
					txt = '';
					break;
				}
				i = txt.lastIndexOf(" ", charsPerLine);
				if (i === 0) {
					txt = txt.substr(1);
					i = charsPerLine;
				} else if (i < 0) {
					i = charsPerLine;
				}

				result.push(txt.substr(0, i));
				txt = txt.substr(i);
				maxDepth-=1;
			}
			if (txt) {
				result.push(result.pop()); //+'...');
			}
			return result;
		};

		mohio.core.makeDivs = function(idArray, d3Parent) {
			d3Parent = d3Parent || d3.select("body");
			var as_CSS_ID_String = function(idArray) {
				if(idArray && idArray.length>0) {
					return "#" + idArray.join(", #");
				}// else
				return "";
			},
			 idstr = as_CSS_ID_String(idArray),
			 divs = d3Parent.selectAll(idstr).data(idArray);

			divs.enter().append("div")
			 .attr("id", function(d) { return d; });
			return divs;
		};

		mohio.core.clearChildren = function(elementID) {
			var sel = d3.select("#"+elementID);
			sel.html("");
			return sel;
		};

		mohio.core.toNum = function(v, def) {
			def = (def===undefined) ? v : def;
			var n = Number(v);
			return (isNaN(n)) ? def : n;
		};


		/**
		 * Exception object for GraphPanel and Graph classes.
		 * @class JsGraphException
		 * @constructs JsGraphException
		 * @param message
		 * @param calleeArgs method arguments causing the exception
		 */
		mohio.core.MUIException = function (message, calleeArgs) {
			var result = new Error(message);
			result.message = message;
			result["arguments"] = calleeArgs; //avoid reserved word
			mohio.log(message);
			return result;
		};

		/**
		 * Exception object for GraphPanel and Graph classes.
		 * @class JsGraphException
		 * @constructs JsGraphException
		 * @param {type} message
		 * @param source the method which originally threw the exception
		 * @param args method arguments causing the exception
		 * @returns {undefined}
		 * @deprecated
		 */
		mohio.core.JsGraphException = function (message, source, args) {
			this.message = message;
			this.source = source;
			this["arguments"] = args; //avoid reserved word
		};

		mohio.core.prompt = function(msg, def) {
			//TODO: make this fancier!
			return prompt(msg, def);
		};

		return mohio;
	};

	return append;
});

