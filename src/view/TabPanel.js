define(["mohio", "src/app/MindMap", "src/app/Matrix",
	"src/app/Planner", "src/app/RoadMap", "d3"],
	function (mohio, addMindMap, addMatrix, addPlanner, addRoadMap) {
	'use strict';
	/*globals d3 */
	var append = function (trm) {
		trm = addMindMap(trm);
		trm = addMatrix(trm);
		trm = addPlanner(trm);
		trm = addRoadMap(trm);
		trm.tabPanel = trm.control || {};

		function hideAllButThis(id, btn) {
			trm.tabDivs.style("display", function () {
				var this_id = d3.select(this).attr("id");
				return (this_id === id || this_id === 'divTabs') ?
				"block" : "none";
			});
			if (btn) {
				d3.selectAll("#divTabs button").attr("class", null);
				btn.attr("class", "selectedTab");
			}
		}

		function makeTabClick(id) {
			return function () {
				var btn = d3.select(this);
				hideAllButThis(id, btn);
				mohio.fire("tabDisplayed", id);
			};
		}

		trm.tabPanel.show = function(divID) {
			var tabDiv = mohio.core.clearChildren(divID);
			tabDiv.attr("class", "controlPane");
			tabDiv.selectAll("button").remove();

			trm.mindMap.makeViews(trm.divMapPanels);
			trm.divIDs.forEach(function (id) {
				tabDiv.append("button")
				.text(id.substr(3))
				.attr("div_id", id)
				.attr("id", id + "Show")
				.attr("class", "tabButton")
				.on("click", makeTabClick(id));
			});
		};

		trm.hideOtherDivs = hideAllButThis;
		return trm;
	};

	return append;
});

