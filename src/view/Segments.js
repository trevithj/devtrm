define(['mohio', 'd3'], function (mohio) {
	'use strict';
	/*globals d3 */

	var append = function (trm) {
		trm.segments = trm.segments || {};

		var showSegments;

		function handleAdd() {
			var src, tgt, seg = trm.graph.getNodes("segment").length+1;
			src = trm.rootNodesMap.get("Market Segments");
			tgt = trm.graph.addNode(src.GUID+"."+seg, "S"+seg, "segment",
				{"name":"New Segment", "index":(seg-1)}
			);
			trm.graph.addEdge(src.GUID, tgt.GUID, "segment");
			showSegments("divSegments");
		}

		function updateCell(td, msg, variable, edge) {
			var newname = mohio.core.prompt(msg, edge.target[variable]);
			if(newname) {
				d3.select(td).text(newname);
				edge.target.setData(variable, newname);
			}
		}

		showSegments = function showSegmentsF(divID) {
			var edges = trm.graph.getEdgesByFieldValue("relType", ["segment"]),
				div = mohio.core.clearChildren(divID),
				tbody, tbl;
			div = div.append("div").attr("id", divID+"-table");
			div.append("h3").text("Market Segments");
			div.append("button").text("Add Segment").on("click", handleAdd);
			tbl = mohio.table.make(div, "matrix");
			mohio.table.populateTableHead(tbl, ["Segment name", "Title", "Index"]);
			tbody = tbl.append("tbody");
			edges.forEach(function(edge) {
				var tr = tbody.append("tr").style("background-color",trm.getColour(edge.target.index));
				tr.append("td").text(edge.target.name)
				.on("dblclick", function() {
					updateCell(this, "Enter new name for this segment", "name", edge);
				});
				tr.append("td").text(edge.target.title)
				.on("dblclick", function() {
					updateCell(this, "Enter new title for this segment", "title", edge);
				});
				tr.append("td").text(edge.target.index);
			});
		};

		trm.segments.show = showSegments;



		return trm;
	};

	return append;
});
