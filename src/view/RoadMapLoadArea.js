/**
 * Handles calculations related to loading charts.
 * Depends on the d3 library, at least version d3.v3.js
 */
define(['d3'], function () {
	'use strict';
	/*globals d3 */
/**
 * @param {Panel} panel
 * @param {Object} trm
 * @returns {unresolved}
 */
	function LoadArea(panel, trm) {
		var area;
		function checkData(d) {
			if(d.x === undefined || d.y === undefined)
			{	throw new Error("Data invalid " + d); }
			if(d.y0 === undefined)
			{	throw new Error("d.y0 not defined " + d); }
		}
		function checkNum(n) {
			if(isNaN(n)) {
				throw new Error("Numerical value expected:" + n);
			}
			return (n);
		}
		area = d3.svg.area()
		.interpolate("step-after")
		.x(function (d) {
			checkData(d);
			return checkNum(panel.xScale(d.x || 0));
		})
		.y0(function (d) { //return 0 if y0 undefined
			checkData(d);
			return checkNum(panel.yScale(d.y0 || 0));
		})
		.y1(function (d) { //return y if y0 undefined
			checkData(d);
			return checkNum(panel.yScale(d.y0 + d.y));
		});


/**
 *
 * @param {Array} layers array of layerObjects of the form:
 *		{name:string, layer:[] }
 *	The layer array contains coordinate objects of the form:
 *		{x:number, y:number, y0:number}
 *	Each layer array should be of the same length. Assume that
 *	the coordinates have already been "stacked".
 * @returns {unresolved}
 */
		this.render = function(layers) {
//			LoadArea.rescaleHeight(layers, panel.yScale());
			//TODO: calculate maxY from y+y0 values in "highest" layer
			var path = panel.theVis().selectAll("path").data(layers);
			path.enter().append("path"); //.select("path")
			path.exit().remove();
			path.attr("class", "loadchart")
			.attr("d", function(l) {
				return area(l.layer);
			})
			.attr("fill", function(l) {
				return trm.getResColour(l.name);
			});
//			console.log(["rendering", panel.xScale().domain()]);
		};


	}

	LoadArea.getMaxLoad = function (layers) {
		function getLoad(d) {return d.y + d.y0; }
		var maxLoad = 0; //calculate the scaled height of the loadchart
		layers.forEach(function(n) {
			var load = d3.max(n.layer, getLoad);
			maxLoad = Math.max(maxLoad, load);
		});
		return maxLoad;
	};

	LoadArea.rescaleHeight = function (layers, yScale) {
		var maxLoad = LoadArea.getMaxLoad(layers); //calculate the scaled height of the loadchart
		yScale.domain([0, Math.ceil(maxLoad)]); //round up
	};

//	return LoadPanel;
	return LoadArea;
});
