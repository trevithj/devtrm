/*
TODO: reconsider how the matrix gets drawn. Use 3 sub-grids instead of 1 grid.
This aids calcs, and ensures the edges are the right way around: that is,
the segment-driver grid shows as driver-segment, so this subgrid actually
needs to be the transpose of the usual grid.
Also, we are creating and storing edges for seg-seg, which we never need and
so shouldn't exist in the underlying model.
*/

define(['mohio', 'MatrixGraph', 'd3'], function (mohio, MatrixGraph) {
	'use strict';
	/*globals d3:false  */
	var append = function (trm) {
		trm.matrix2 = trm.matrix2 || {};
		var theTable, segsMx, userMx, calcMx,
				  segsGrid, userGrid, calcGrid;

		function getMxObj(srcNodeVType, tgtNodeVType) {
			var obj = trm.matrix.getNodeEdges(srcNodeVType, tgtNodeVType);
			return MatrixGraph.toMatrixObject(obj.edges);
		}

		function calcSumOfProducts(segWts, usrWts) {
			var sumProduct = 0;
			usrWts.forEach(function(df, i) {
				var w1 = 0, w2 = 0;
				w1 -= df.getData("weight");
				w2 -= segWts[i].getData("weight");
				sumProduct += (w1 * w2);
			});
			return sumProduct;
		}

		function normalizeColumn(calcCol) {
			var max = 0;
			calcCol.forEach(function(edge) {
				max = Math.max(max, edge.getData("weight"));
			});
			calcCol.forEach(function(edge) {
				var val = edge.getData("weight")/max;
				edge.setData("weight", val*10);
			});
			return calcCol;
		}

		function calculateDerivedWeightsColumn(segRow, usrRowsGrid, calcCol) {
			calcCol.forEach(function(edge, i) {
				var sumProducts = calcSumOfProducts(segRow, usrRowsGrid[i]);
				edge.setData("weight", sumProducts);
			});
			return normalizeColumn(calcCol);
		}
		trm.matrix2.testCalc = calculateDerivedWeightsColumn;//temp

		function getEdgeOrThrow(edge) {
			if(edge) { return edge;}
			throw new Error("Edge is undefined");
		}

		function refreshCalculatedFields() {
			theTable.selectAll("td.segCol-cell").text(function(d) {
				return d3.round(d.getData("weight"),2);
			});
		}

		function calculateNormalizedWeights() {
			var maxSeg = segsGrid.length -1;
			segsGrid.forEach(function(segRow, i) {
				var colIndex = maxSeg - i,
				calcCol = calcMx.column(colIndex, getEdgeOrThrow);
				calcCol = calculateDerivedWeightsColumn(segRow, userGrid, calcCol);
			});
			refreshCalculatedFields();
		}

		function overrideSetClick(td) {
			var doClick = function(d) {
				var flag = trm.matrix.checkEdge(d);
				if(flag==="") {
					trm.matrix.updateWeightCell(this, "Enter new weight", d);
					calculateNormalizedWeights();
				}//else ignore
			};
			td.on("dblclick", doClick);
		}

		function toStringFn(e) {
			//return e.getData("weight");
			var ret=null, flag = trm.matrix.checkEdge(e);
			if(flag==="ST") {
				ret = " ";
			} else if (flag==="T") {
				ret = "fixd";
			}
			return ret || d3.round(e.getData("weight"),2);
		}

		function getNodeTitle(node) { return (node) ? node.title : "??"; }

		function makeMockEdge(edge, node, tgtVType) {
			var e = {source:{}, target:{}};
			e.source.visualType=node.visualType;
			e.source.index=edge.source.index;
			e.target.index=node.index;
			e.target.visualType=tgtVType;
			return e;
		}

		function writeSegsGrid(tbody) {
			var rowHeads = segsMx.colHeads(getNodeTitle), //sideways matrix
			nonSegColCount = segsGrid[0].length;
			segsGrid.forEach(function (row, r) {
				row = row.concat(); //copy
				calcMx.srcNodes.forEach(function(n, c) { //add seg-seg columns
					row.push(makeMockEdge(row[c],n,"segment"));
				});
				var tr = tbody.append("tr");
				tr.append("th").text(rowHeads[r]); //add row heading to grid rows
				/*jslint unparam:true */
				mohio.table.addCellsToRow(tr, row,
					toStringFn, null, function(e, i) {
						return (i<nonSegColCount) ? "segRow-cell" : "segCnr-cell";
					}
				);
			});
		}

		function writeCalcGrid(tbody) {
			var rowHeads = userMx.rowHeads(getNodeTitle);
			userGrid.forEach(function (row, r) {
				row = [].concat(row, calcGrid[r]);
				var tr = tbody.append("tr");
				tr.append("th").text(rowHeads[r]); //add row heading to grid rows
				/*jslint unparam:true */
				mohio.table.addCellsToRow(tr, row,
					toStringFn, overrideSetClick, function(e, i) {
						return (i<userGrid[r].length) ? "entry-cell" : "segCol-cell";
					}
				);
			});
		}

		function writeGrids(tbl) {
			trm.matrix.sortNodes(calcMx.srcNodes, true);
			trm.matrix.writeColumnHeaders(tbl, segsMx.rowHeads, calcMx.colHeads);
			var tbody = tbl.append("tbody");
			segsGrid = segsMx.transpose(getEdgeOrThrow);
			userGrid = userMx.grid(getEdgeOrThrow);
			calcGrid = calcMx.grid(getEdgeOrThrow);
			writeSegsGrid(tbody);
			writeCalcGrid(tbody);
		}

		trm.matrix2.showCombinedMatrix = function(divID, colType, rowType, title) {
			var div = mohio.core.clearChildren(divID);
			segsMx = getMxObj("segment", colType);
			userMx = getMxObj(colType, rowType);
			calcMx = getMxObj("segment", rowType);
			div.append("h3").text(title); //"Matrix2: feature - driver weightings");
			theTable = div.append("table").attr("class", "matrix");
			writeGrids(theTable);
			theTable.selectAll("td")
			.style("background-color", trm.matrix.getCellColour);
			calculateNormalizedWeights();
		};

		trm.matrix2.show = function(divID) {
			trm.matrix2.showCombinedMatrix(
				divID, "driver", "feature",
				"Matrix2: feature - driver weightings"
			);
		};


		return trm;
	};

	return append;
});
