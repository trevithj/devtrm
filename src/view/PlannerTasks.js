define(['mohio', 'src/model/ObjectTableModel', 'd3'], function (mohio, makeTableModel) {
	'use strict';
	/*globals d3:false  */
	var append = function (trm) {
		trm.planner = trm.planner || {};
		var tasks = {};

		function getTaskNodesTM() {
			var nodes = trm.graph.getNodes("task");
			return makeTableModel(nodes, ["type","title","duration"]);
		}
		function formatTaskTable(tbl) {
			var rows = tbl.select("tbody").selectAll("tr");
			rows.selectAll("td")
			.attr("class", "entry-cell")
			.on("click", function() {
				trm.planner.highlightRow(d3.select(this), tbl);
			})
			.on("dblclick", function(d,i) {
				if(i>0) {
					trm.planner.updateCell(d3.select(this), d, "Update the cell");
				}
			});
			rows.each(function(d) {
				var node = d[0].data(),
					cell1 = this.childNodes[0],
					colr = trm.getColour(node.index);
				d3.select(cell1).style("background-color", colr);
			});
		}
		function addTasksTable(div, divID) {
			var tm, tbl, div1 = div.append("div").attr("id", divID+"-tableTsk");
			div1.append("h4").text("Tasks");
			tbl = mohio.table.make(div1, "matrix");
			tm = getTaskNodesTM();
			mohio.table.populateTableHead(tbl, tm.getHeadings());
			mohio.table.populateTableBody(tbl, tm.getGrid(false));
			formatTaskTable(tbl);
			tasks.table = tbl;
			return tbl;			
		}
		
		tasks.addTable = addTasksTable;

		trm.planner.tasks = tasks;
		return trm;
	};

	return append;
});
