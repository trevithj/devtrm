define(['mohio', 'MatrixGraph', 'd3'], function (mohio, MatrixGraph) {
	'use strict';
	/*globals d3:false  */
	var append = function (trm) {
		trm.planner = trm.planner || {};
		var allocs = {};
		
		function makeTableHeads(tbl) {
			var heads = ["Task"], resNodes = trm.graph.getNodes("resource");
			resNodes.forEach(function(res) {
				heads.push(res.name);
			});
			mohio.table.populateTableHead(tbl, heads);
			tbl.selectAll("th").style("border", "thin solid silver");
			allocs.resNodes = resNodes;
		}

		function showRoundedWeights(d) {
			var wt = d.weight;
			return (isNaN(wt)) ? "??" : d3.round(wt,2);
		}		
		function makeTableGrid(tbl) {
			allocs.TaskNodes = trm.graph.getNodes("task");
			var mx, defProps = {weight:0},
			edges = trm.graph.fullyConnect(allocs.resNodes, allocs.TaskNodes, "res-task", defProps);
			mx = MatrixGraph.toMatrixObject(edges);
			trm.matrix.writeGrid(mx, tbl, showRoundedWeights, null, true);
		}
		
		function addAllocTable(div, divID) {
			var tbl, div1 = div.append("div").attr("id", divID+"-tableAlloc");
			div1.append("h4").text("Resource Allocation");
			tbl = mohio.table.make(div1, "matrix");
			makeTableHeads(tbl);
			makeTableGrid(tbl);
			tbl.selectAll("td").attr("class", "entry-cell");
		}
		allocs.addTable = addAllocTable;
		trm.planner.allocs = allocs;
		return trm;
	};

	return append;
});
