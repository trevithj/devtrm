define(['mohio', 'MatrixGraph', 'd3'], function (mohio, MatrixGraph) {
	'use strict';
	/*globals d3:false  */
	var append = function (trm) {
		trm.planner = trm.planner || {};
		var solutions = {};

		function addControlButtonColumn(mx, tbl, title, fn) {
			tbl.select("thead").select("tr").append("th").text("add");
			tbl.select("tbody").selectAll("tr").each(function(tr, i) {
				var btn = d3.select(this).append("td").append("button");
				btn.text(title);
				if(fn) {
					btn.on("click", function() {
						fn.call(this, mx.tgtNodes[i], title);
					});
				}
			});
		}
		function addTask(solNode, type) {
			var tgt, guid = "Task"+new Date().getTime(),
				props = {"start":0, "duration": 1, "type": type, "index": solNode.index};
			tgt = trm.graph.addNode(guid, "New task", "task", props);
			trm.graph.addEdge(solNode.GUID, tgt.GUID, "sol-task");
			//refreshTasksTable
			trm.planner.show();
		}

		function getWeightedSolutionsMatrix(){
			var tgtNodes = trm.graph.getNodes("solution"),
				srcNodes = trm.graph.getNodes("segment"),
				defProps = {visualType:"segCol-cell"},
				edges;
			edges = trm.graph.fullyConnect(srcNodes, tgtNodes, "seg-solution", defProps);
			return MatrixGraph.toMatrixObject(edges);
		}
		
		function doNothing() {}

		function showRoundedWeights(d) {
			var wt = d.weight;
			return (isNaN(wt)) ? "??" : d3.round(wt,2);
		}

		function addSolutionsTable(div, divID) {
			var div1, tbl, mx;
			div1 = div.append("div").attr("id", divID+"-tableSol");
			div1.append("h4").text("Solutions");
			tbl = mohio.table.make(div1, "matrix");
			mx = getWeightedSolutionsMatrix();
			trm.matrix.writeGrid(mx, tbl, showRoundedWeights, doNothing);
			tbl.selectAll("td")
			.attr("class", "segCol-cell")
			.style("background-color", function(edge) {
				return trm.getColour(edge.target.index);
			});
			tbl.selectAll("td").on("click", function() {
				trm.planner.highlightRow(d3.select(this), tbl);
			});
			addControlButtonColumn(mx, tbl, "Apply", addTask);
			addControlButtonColumn(mx, tbl, "Resrch", addTask);
			addControlButtonColumn(mx, tbl, "Procur", addTask);
			return tbl;
		}


		solutions.addTable = addSolutionsTable;

		trm.planner.solutions = solutions;
		return trm;
	};

	return append;
});
