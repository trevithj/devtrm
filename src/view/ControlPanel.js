define(['mohio', 'd3'], function (mohio) {
	var pl     = /\+/g;  // Regex for replacing addition symbol with a space
	var search = /([^&=]+)=?([^&]*)/g;
	function decode(s) { 
		return decodeURIComponent(s.replace(pl, " ")); 
	}

	function getJsonFromUrl(query) {
		var result = {};
		var match;
		while (match = search.exec(query)) {
			result[decode(match[1])] = decode(match[2]);
		}
		return result;
	}

	var append = function (trm) {
		trm.control = trm.control || {};
		var buttons = ["New","Load","Save"],
			divID = trm.divSpecial[0];

		trm.control.show = function() {
			var div = mohio.core.clearChildren(divID);
			div.selectAll("button").data(buttons).enter()
			.append("button")
			.text(function(d) {return d; })
			.on("click", function(d) {
				mohio.fire("clicked"+ d);
			});
		};

		trm.control.getJsonFromUrl = getJsonFromUrl;

		var params = getJsonFromUrl(window.location.search.substr(1));
		params.fn = params.fn || "saved";
		trm.control.lastLoadedFileName = params.fn;
		trm.control.lastSavedFileName = params.fn;

		return trm;
	};

	return append;
});