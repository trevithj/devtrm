define(['mohio','d3'], function (mohio) {
	'use strict';
	/*globals d3:false  */
	var append = function (trm) {
		var mapVType="driver"; //divID = trm.divIDs[1],
		trm.mindMap1 = trm.mindMap1 || {};
		trm.mindMap1.view = null; //to be set externally
		

		function updateCell(tr, node) {
			tr = d3.select(tr);
			node = node || tr.data()[0]; //.__data__;
			var newName = mohio.core.prompt("Enter a new title for the driver", node.title);
			if(newName) {
				node.setData("title", newName);
				tr.select("td.col0").text(node.title);
			}
		}

		function refreshTable() {
			return trm.mindMap.refreshTable(trm.mindMap1.view, "Drivers", mapVType, updateCell);
		}


		function addDriver() {
			var node, guid = "Driver",
				index = trm.graph.getNodes(mapVType).length+1;
			node = trm.graph.addNode(guid+"."+index, "D"+index, mapVType);
			node.setData("index", index-1);
			refreshTable();
		}


		trm.mindMap1.show = function() {
			var mmap = trm.mindMap, nodes, edges,
				panel = trm.mindMap1.view.panel;
			panel.theVis().selectAll("g").remove();
			trm.mindMap.panel = panel;
			nodes = mmap.getMapNodes("map1Root");
			edges = mmap.getMapEdges(["map-text1"]);
			trm.mindMap.edges = edges;

			mmap.showMap(panel, nodes.all, edges)
			.on("click", function(d) {
				if (d3.event.shiftKey) {
					trm.mindMap.addChildNode(d, "map-text1", panel);
				} else {
					trm.mindMap.handleNodeClick(d);
				}
				mohio.fire("map1NodeChanged");
			})
			.call(trm.mindMap.drag);

			refreshTable().tableDiv.select("button").text("Add Driver")
			.on("click", addDriver);
		};
		
		mohio.bind("map1NodeChanged", trm.mindMap1.show);


		return trm;
	};

	return append;
});
