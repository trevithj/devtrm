/* https://github.com/mbostock/d3/wiki/Force-Layout
# force.nodes([nodes])

If nodes is specified, sets the layout's associated nodes to the specified array.
If nodes is not specified, returns the current array, which defaults to the empty array.
Each node has the following attributes:
 index - the zero-based index of the node within the nodes array.
 x - the x-coordinate of the current node position.
 y - the y-coordinate of the current node position.
 px - the x-coordinate of the previous node position.
 py - the y-coordinate of the previous node position.
 fixed - a boolean indicating whether node position is locked.
 weight - the node weight; the number of associated links.

These attributes do not need to be set before passing the nodes to the layout;
 if they are not set, suitable defaults will be initialized by the layout when
 start is called. However, be aware that if you are storing other data on your
 nodes, your data attributes should not conflict with the above properties used
 by the layout.
 */
define(['mohio', 'd3'], function(mohio) {
	'use strict';
	/*globals d3: false */

	var visualNode = {},
		addIfNotThereAlready = mohio.core.addChildIfNotThereAlready;

	function getFill(node) {
		return (node.index===undefined) ? "#F8F8F8"
			: mohio.trm.getColour(node.index);
	}

	function isMapRootNode(node) {
		return (
			node.visualType === "map1Root" ||
			node.visualType === "map2Root" ||
			node.visualType === "map3Root"
		);
	}

	function fitEllipseToText(d3Ellipse, d3Text) {
		var textSize = d3Text.node().getBBox();
		d3Ellipse
		.attr("rx", Math.round(textSize.width * 0.7))
		.attr("ry", Math.round(textSize.height * 0.7));
		return textSize;
	}
	function fitRectToText(d3Rect, d3Text) {
		var textSize = d3Text.node().getBBox();
		d3Rect
		.attr("x", textSize.x-4)
		.attr("y", textSize.y-2)
		.attr("width", textSize.width+8)
		.attr("height", textSize.height+4);
		return textSize;
	}

	function writeText(g, node) {
		var gText, txt = node.getDisplayText();
		gText = g.select("text")
		.attr("text-anchor", "middle");
		txt = txt.split("|");
		var update = gText.selectAll("tspan").data(txt);
		update.enter().append("tspan");
		update.exit().remove();
		update
		.attr("dy", "1.1em")
		.attr("x", 0)
		.text(function(d) { return d; });
		return gText;
	}

	function moveNode(g, x, y, box) {
		box = box || g.node().getBBox();
		var xy = [d3.round(x,2), d3.round(y-(box.height/2),2)];
		g.attr("transform", "translate(" + xy + ")");
	}

	function drawNodeShape(g, node) {
		var shape = addIfNotThereAlready(g, "rect");
		shape
		.attr("rx", 8)
		.attr("ry", 8)
		.attr("stroke", (node.selected) ? "black" : "silver");
		if (node.visualType !== "text") { //assume map root
			shape.attr("fill", "white");
		} else {
			shape.attr("fill", node.getFill());
		}
	}

	function drawNode(g, node) {
		drawNodeShape(g, node);
		addIfNotThereAlready(g, "text");
		var gText = writeText(g, node);
		var box = fitRectToText(g.select("rect"), gText);
		moveNode(g, node.x, node.y, box);
	}


	function editText(g, node) {
		var txt = mohio.core.prompt("Editing node text", node.text);
		if(txt) {
			node.setData("text", txt);
			writeText(g, node);
		}
		return false;
	}

	/* Functors */
	function returnTrue() { return true; }
	function returnFalse() { return false; }
	function returnType() { return "[object VisualNode]"; }
	function doNothing() {}

	function augmentRootNode(dataNode) {
		dataNode.isMapRootNode = returnTrue;
		dataNode.getDisplayText = function() {
			return this.name || this.title;
		};
		dataNode.getFill = function() {
			return "black";
		};
		dataNode.editText = doNothing;
	}

	function augmentTextNode(dataNode) {
		dataNode.isMapRootNode = returnFalse;
		dataNode.getDisplayText = function() {
			return this.text || this.title;
		};
		dataNode.getFill = function() {
			return getFill(this);
		};
	}

	/**
	 * Augment a dataNode object.
	 * @param {object} dataNode instance.
	 * @returns {object} object that augments the dataNode with view-specific values.
	 */
	function augment(dataNode) {
		var defaultG = null;
		//use polymorphism - augment different methods depending on the node
		if (isMapRootNode(dataNode)) {
			augmentRootNode(dataNode);
		} else {
			augmentTextNode(dataNode);
			dataNode.editText = function(g) {
				g = g || defaultG;
				defaultG = g;
				return editText(g, this);
			};
		}
		dataNode.draw = function(g) {
			g = g || defaultG;
			defaultG = g;
			return drawNode(g, dataNode);
		};
		dataNode.toString = returnType;

//		dataNode.isTagNode = function() {
//			return isTagNode(this);
//		};

		return dataNode;
	}

	/**
	 * Convenience method to augment an array of nodes.
	 * @param {Array} dataNodes
	 * @returns {Array}
	 */
	function convert(dataNodes) {
		dataNodes.forEach(function(dataNode) {
			augment(dataNode);
		});
		return dataNodes;
	}


	function drawAll(gNodes) {
		gNodes.attr("class", function(n) {
			return "node " + n.visualType;
		});
		gNodes.each(function(d) { //expect d to be a VisualNode
			d.draw(d3.select(this));
		});
	}

	/*--- return object --*/
	visualNode.augment = augment;
	visualNode.convert = convert;
	visualNode.draw = drawAll;
	visualNode.fitEllipseToText = fitEllipseToText;
	visualNode.fitRectToText = fitRectToText;
	visualNode.move = moveNode;

	return visualNode;
});
