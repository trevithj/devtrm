define(['mohio','d3'], function (mohio) {
	'use strict';
	/*globals d3:false  */
	var append = function (trm) {
		var mapVType="feature"; //divID = trm.divIDs[4],
		trm.mindMap2 = trm.mindMap2 || {};
		trm.mindMap2.view = null; //to be set externally


		function updateCell(tr, node) {
			tr = d3.select(tr);
			node = node || tr.data()[0]; //.__data__;
			var newName = mohio.core.prompt("Enter a new title for the feature", node.title);
			if(newName) {
				node.setData("title", newName);
				tr.select("td.col0").text(newName);
			}
		}

		function refreshTable() {
			return trm.mindMap.refreshTable(trm.mindMap2.view, "Features", mapVType, updateCell);
		}


		function addFeature() {
			var node, guid = "Feature",
				index = trm.graph.getNodes(mapVType).length+1;
			node = trm.graph.addNode(guid+"."+index, "F"+index, mapVType);
			node.setData("index", index-1);
			refreshTable();
		}


		trm.mindMap2.show = function() {
			var mmap = trm.mindMap, nodes, edges,
				panel = trm.mindMap2.view.panel;
			panel.theVis().selectAll("g").remove();
			trm.mindMap.panel = panel;
			nodes = mmap.getMapNodes("map2Root");
			edges = mmap.getMapEdges(["map-text2"]);
			trm.mindMap.edges = edges;

			mmap.showMap(panel, nodes.all, edges)
			.on("click", function(d) {
				if (d3.event.shiftKey) {
					trm.mindMap.addChildNode(d, "map-text2", panel);
				} else {
					trm.mindMap.handleNodeClick(d);
				}
				mohio.fire("map2NodeChanged");
			})
			.call(trm.mindMap.drag);

			refreshTable().tableDiv.select("button").text("Add Feature")
			.on("click", addFeature);
		};

		mohio.bind("map2NodeChanged", trm.mindMap2.show);

		return trm;
	};

	return append;
});
