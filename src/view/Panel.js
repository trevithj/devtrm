define(['jquery', 'd3'], function ($) {
//pass explicit jquery ref, but d3 isn't a module. Both are defined globally anyway.
	'use strict';
	/*globals d3 */
	/**
	 * Panel defines a collection of svg elements in a div,
	 * that can handle panning, zooming and brushing actions.
	 * Structure of svg elements under the div is:
	 * svg.theSVG
	 * ->rect.bg
	 * ->g.theVis
	 * @param {string} parentDivID
	 *  Unique identifier for a (presumed) existing div element.
	 * @param {Object} scales Optional object that contains externally-set
	 *  d3.scale objects to override defaults. If either/both of scales.x or
	 *  scales.y is undefined, the internal scales default to d3.scale.linear.
	 * @returns {Panel.panel}
	 * @requires JQuery, d3
	 */
	function Panel (parentDivID, scales) {
		if(!parentDivID) {
			throw new Error("Undefined parameter");
		}
		scales = scales || {x:null, y:null};
		var theDiv, theDivD3, theSVG, theBG, theVis,
			colour = "white", panel = {};
		scales.x = scales.x || d3.scale.linear();
		scales.y = scales.y || d3.scale.linear();

		/**jquery wrapper for the parent div for accessing changes in width and height*/
		theDiv = $('#' + parentDivID);           //d3 is inconsistent in its support

		/*d3 wrapper for the same parent div. Use this to detect zoom actions*/
		theDivD3 = d3.select('#' + parentDivID); //
		if(theDivD3.empty()) {
			throw new Error([this, parentDivID, "Panel div init failed"]);
		}
		/*the top svg element for this panel. Any content added to this will be
		 outside the control of the zoom/pan/transform. See theVis*/
		(function (id) {
			theSVG = d3.select("#"+id);//
			if (theSVG.empty()) { //doesn't exist, so create it
				theSVG = theDivD3.append('svg')
				.attr('width', '100%')
				.attr('height', '100%')
				.attr('id', id);
			}
		}(parentDivID + "_theSVG"));

		/*the background rectangle. Use this to set background colour*/
		(function (id) {
			theBG = theDivD3.select("#"+id);    //the background rectangle
			if (theBG.empty()) { //doesn't exist, so create it
				theBG = theSVG.append('svg:rect')
				.attr('width', '100%')
				.attr('height', '100%')
				.attr('id', id)
				.style('fill', colour);
			}
		}(parentDivID + "_theBG"));

		/*the top-level group element for applying zooming/panning*/
		(function (id) {
			theVis = d3.select("#"+id);  //the top-level group element for zooming/panning
			if (theVis.empty()) { //doesn't exist, so create it
				theVis = theSVG.append('svg:g').attr('id', id);
			}
		}(parentDivID + "_theVis"));

		/**expose the jquery div methods - setters/getters for the div size
		 * @returns {int}
		 */
		panel.width = function () {
			return theDiv.width();
		};
		/**Height of parent div in pixels
		 * @returns {int}
		 */
		panel.height = function () {
			return theDiv.height();
		};
		/**allow read-only access to the internal objects
		 * @returns {d3.selection(svg:rect)} background rectangle
		 */
		panel.theBG = function () {
			return theBG;
		};
		/**
		 * @returns {jQuery(div)}
		 */
		panel.theDiv = function (withD3Wrapper) {
			return (withD3Wrapper) ? theDivD3 : theDiv;
		};
		/**
		 * @returns {d3.selection(svg}
		 */
		panel.theSVG = function () {
			return theSVG;
		};
		/**
		 * @returns {d3.selection(svg:g)} top-level group
		 */
		panel.theVis = function () {
			return theVis;
		};

	//		mohio.debug(panel);


		/**Returns either the scaled value of domainX, or
		 * if domainX isn't defined, returns the x scale.
		 * @param {type} domainX
		 * @returns {Number|d3.scale}
		 */
		function xScale(domainX) {
			return (domainX === undefined) ? scales.x : scales.x(domainX);
		}
		panel.xScale = xScale;

		/**Returns either the scaled value of domainY, or
		 * if domainX isn't defined, returns the y scale.
		 * @param {type} domainY
		 * @returns {Number|d3.scale}
		 */
		function yScale(domainY) {
			return (domainY === undefined) ? scales.y : scales.y(domainY);
		}
		panel.yScale = yScale;

		/** Calculate a scaled interval between two domain-scale units
		 * @param {type} domX1
		 * @param {type} domX2
		 * @returns {Number} interval in range units
		 */
		panel.xDim = function (domX1, domX2) {
			return (xScale(domX1) - xScale(domX2));
		};

		/** Calculate a scaled interval between two domain-scale units
		 * @param {type} domY1
		 * @param {type} domY2
		 * @returns {Number} interval in range units
		 */
		panel.yDim = function (domY1, domY2) {
			return (yScale(domY1) - yScale(domY2));
		};

		/**Convenient method that converts range coordinates
		 * into domain coordinates.
		 * Note that the domain units are not necessarily numeric.
		 * @param {Array} rangeXY coordinates [x,y] in range units (usually pixels).
		 * @returns {Array} xy coordinates [x,y] in domain units.
		 */
		panel.asDomain = function (rangeXY) {
			return [
				xScale().invert(rangeXY[0]),
				yScale().invert(rangeXY[1])
			];
		};

		/**Convenient method that converts domain coordinates
		 * into range coordinates.
		 * Note that the domain units are not necessarily numeric.
		 * @param {Array} domainXY coordinates [x,y] in domain units.
		 * @returns {Array} xy coordinates [x,y] in range units (usually pixels).
		 */
		panel.asRange = function (domainXY) {
			return [
				Math.round(xScale(domainXY[0])),
				Math.round(yScale(domainXY[1]))
			];
		};



		panel.doPointerEvents = function(flag) {
			var val = (flag) ? null : "none";
			theSVG.style("pointer-events", val);
		};
		/*
		panel.getScales = function() {
			return {
				"xScale":xScale,
				"yScale":yScale
			};
		};
		*/
		/** Calculate center of the parent element, in pixels.
		 * Corresponds to clientX, clientY coordinate system.
		 *@return {array} 2d coordinate array [x,y] (usually in pixels)
		 */
		panel.getCenter = function () {
			return [
				Math.round(this.width() / 2),
				Math.round(this.height() / 2)
			];
		};

		/** Calculate the top-left corner of the screen, in domain units.
		 * That is, returns the domain coordinates that would map to the
		 * range coordinates of the parent div.
		 *@return {array} 2d coordinate array [x,y]
		 */
		function getNW() {
			return [
				xScale().invert(0),
//				yScale().invert(theDiv.height()-1) //allow for y-scale inversion
				yScale().invert(0) //allow for y-scale inversion
			];
		}

		/** Calculate the bottom-right corner of the screen, in domain units.
		 * That is, returns the domain coordinates that would map to the
		 * range coordinates of the parent div.
		 *@return {array} 2d coordinate array [x,y]
		 */
		function getSE() {
			return [
				xScale().invert(theDiv.width()-1),
//				yScale().invert(0) //allow for y-scale inversion
				yScale().invert(theDiv.height()-1)
			];
		}

	/**Returns bounds of div size (x1,y1,x2,y2)
	 * @returns {Panel.panel.getDimensions.Anonym$0}
	 */
		panel.getDimensions = function() {
			var nw = getNW(), se = getSE();
			return {
				"x1": nw[0],
				"y1": nw[1],
				"x2": se[0],
				"y2": se[1]
			};
		};
		
		/**Moves the display by shifting the scale domains by the given amounts.
		 * Note, this assumes domain values are numeric. If not, this method will
		 * need to be overloaded to handle different domains (dates, ordinal, etc).
		 * @param {Array} delta [dx, dy] offsets by which to pan.
		 * @returns {Boolean} true if pan is successful, else false;
		 */
		panel.panBy = function(delta) {
			if(delta instanceof Array) {
				var domX = xScale().domain(),
						domY = yScale().domain();
				domX[0]-=delta[0];
				domX[1]-=delta[0];
				domY[0]-=delta[1];
				domY[1]-=delta[1];
				xScale().domain(domX);
				yScale().domain(domY);
				return true;
			} //else
			return false;
		};

		function setScaleMargins(scale, lowMargin, hiMargin) {
			lowMargin = lowMargin || 0;
			hiMargin = hiMargin || 0;
			var r = scale.range();
			r[0] += lowMargin;
			r[1] -= hiMargin;
			scale.range(r);
		}
		/**
		 * Shrinks the xScale range by the given amounts in pixels.
		 * @param {type} leftMarginWidth in pixels
		 * @param {type} rgtMarginWidth in pixels
		 * @returns {undefined}
		 */
		panel.setHorizontalMargins = function(leftMarginWidth, rgtMarginWidth) {
			setScaleMargins(xScale(), leftMarginWidth, rgtMarginWidth);
		};


		/** Helper method to easily reset ranges of both scales.
		 * Sets ranges equal to the dimensions of theDiv in pixels.
		 * @returns {undefined}
		 */
		panel.setRangeToDiv = function () {
			//recompute range of both scales
			xScale().range([0, theDiv.width()-1]);
			yScale().range([0, theDiv.height()-1]);
		};

		/**Recalculate the x and y scales based on the size
		 *  of containing div, so ranges always reflect div size.
		 * This should be called on window resize events, etc.
		 */
		panel.resize = function () {
			panel.setRangeToDiv();
		};

	/**Helper method to easily set domains of both scales.
	 * Sets domains to match relevant ranges.
	 * @param {Boolean} notCentered By default the domains are offset
	 * so that a domain coordinate of [0,0] maps to the div center.
	 * If this param is true, domains equal ranges.
	 * @returns {undefined}
	 */
		panel.setDomainToRange = function (notCentered) {
			//recompute domain of both scales
			if(notCentered) {
				xScale().domain(xScale().range());
				yScale().domain(yScale().range());
			} else {
				var ctr = this.getCenter();
				xScale().domain([-ctr[0], +ctr[0]]);
				yScale().domain([-ctr[1], +ctr[1]]);
			}
		};

	/**
		panel.domainZoom = function(xDom, yDom) {
			console.log([xDom, yDom]);
			xScale().domain(xDom);
			yScale().domain(yDom);
		};
	**/

		function zoom() {
			panel.theVis().attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
		}

		//Final initializing
		panel.theBG().call(d3.behavior.zoom().scaleExtent([0.125, 8]).on("zoom", zoom));
		panel.setRangeToDiv();
		d3.select(window).on("resize", panel.resize);
		return panel;
	}

	return Panel;
});
