define(['mohio', 'src/model/LoadBlocks','src/view/RoadMapLoadArea', 'd3'],
 function (mohio, LoadBlocks, LoadArea) {
	'use strict';
	/*globals d3:false  */
	var append = function (trm) {
		trm.roadMap = trm.roadMap || {};
		var leftMargin = 200,
			heightFactor = 0.7,
			vis = null,
			resources = null,
			panel = null,
			loadBlocks = new LoadBlocks(),
			loadArea = null; //new LoadsArea();


		function drawResourceHead(resRowScale) {
			var height = resRowScale(1) - resRowScale(0),
				width = Math.round(leftMargin/2);
			return function(sel) {
				sel.append("rect")
					.attr("x", -leftMargin)
					.attr("y", function(d, i) {
						return resRowScale(i, d);
					})
					.attr("width", width)
					.attr("height", height)
					.attr("fill", function(d) {
						return trm.getResColour(d.index);
					});
				sel.append("text")
					.attr("x", -leftMargin)
					.attr("y", function(d, i) {
						return resRowScale(i+heightFactor, d);
					})
					.text(function(d) { return d.name; });
			};
		}

		function drawResourceHeads() {
			var resRowScale, heads = vis.selectAll("g.resHead").data(resources);
			resRowScale = d3.scale.linear();
			resRowScale.range(panel.yScale().range());
			resRowScale.domain([0, resources.length]);
			heads.enter().append("g").attr("class", "resHead");
			heads.exit().remove();
			heads.call(drawResourceHead(resRowScale));
		}

		function buildLoadBlocks() {
			resources.forEach(function(res) {
				var capacity = mohio.core.toNum(res.capacity, 1);
				res.outEdges.forEach(function(e) {
					if(e.weight > 0 && e.target.visualType==="task") {
						var load, from, till, tgt = e.target;
						load = mohio.core.toNum(e.weight) / capacity;
						from = mohio.core.toNum(tgt.start);
						till = trm.roadMap.calcEnd(tgt.start, tgt.duration);
						loadBlocks.addBlock(res.index, load, from, till);
					}
				});
			});
		}
		
		function updateLoadYAxis(panel) {
			var gy, width, axis = d3.svg.axis();
			width = panel.xScale().range()[1];
			axis.scale(panel.yScale())
			.tickSize(1)
			.orient("left");
			gy = panel.theVis().selectAll("g.yAxis").data([1]);
			gy.enter().append("g").attr("class", "yAxis");
			gy.exit().remove();
			gy.call(axis);
			gy.selectAll("g.tick line")
				.attr("stroke", "silver")
				.attr("x2", width);
		}

		function showLoads() {
			vis = trm.roadMap.loadPanel.theVis();
			panel = trm.roadMap.loadPanel;
			resources = trm.graph.getNodes("resource");
			loadBlocks.reset();
			loadArea = new LoadArea(panel, trm);

			panel.yScale().domain([10, 0]);
			//set scales to match the taskPanel
			panel.xScale().range(trm.roadMap.taskPanel.xScale().range());
			panel.xScale().domain(trm.roadMap.taskPanel.xScale().domain());
			
			drawResourceHeads();
			buildLoadBlocks();
			var layers = loadBlocks.getLayers();
			panel.yScale().domain([LoadArea.getMaxLoad(layers)+0.5,0]);
			loadArea.render(layers, loadBlocks.getNames());
			updateLoadYAxis(panel);
		}

		trm.roadMap.showLoads = showLoads;
		return trm;
	};

	return append;
});
