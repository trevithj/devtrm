define(['mohio', 'src/app/TaskTraverse','d3'],
 function (mohio, getTraverser) {
	'use strict';
	/*globals d3:false  */
	var append = function (trm) {
		trm.roadMap = trm.roadMap || {};
		var selectedTask = null,
			leftMargin = 200,
			topBlockMargin = 5,
			heightFactor = 0.7,
			tasks = null,
			vis = null,
			blocks = null,
			traverse = getTraverser(trm.roadMap),
			blockDrag = d3.behavior.drag();

		function calcEnd(start, duration) {
			return mohio.core.toNum(start) + mohio.core.toNum(duration);
		}

		function getTaskRowY(i) {
			return trm.roadMap.taskPanel.yScale(i);
		}

		function getTimeX(days) {
			days = mohio.core.toNum(days);
			return trm.roadMap.taskPanel.xScale(days);
		}
		function getDaysFromX(x) {
			x = mohio.core.toNum(x);
			return trm.roadMap.taskPanel.xScale().invert(x);
		}
	

		function updateTaskPosition(task, i) {
			var dim = {};
			dim.y = i;
			dim.sy1 = getTaskRowY(i);
			dim.syc = getTaskRowY(i + 0.5);
			dim.x1 = task.start;
			dim.x2 = calcEnd(task.start, task.duration);
			dim.sx1 = getTimeX(dim.x1);
			dim.sx2 = getTimeX(dim.x2);
			task.dim = dim;
		}
		function updateTaskPositions() {
			tasks.forEach(updateTaskPosition);
		}


		function addDependencyLink(srcTask, tgtTask) {
			var reverse, existing;
			reverse = trm.graph.findLink(tgtTask.GUID, srcTask.GUID);
			if(reverse) {
				alert("This would create a circular dependency - link not added.");
			} else {
				existing = trm.graph.findLink(srcTask.GUID, tgtTask.GUID);
				if(existing===null) {
					trm.graph.addEdge(srcTask.GUID, tgtTask.GUID, "task-task");
				} else {
					trm.graph.deleteEdge(srcTask.GUID, tgtTask.GUID);
				}
				traverse.forward(srcTask);
				trm.roadMap.show();
			}
		}


		function setDisplayDaysWidth() {
			var daysWidth, dMax = d3.max(tasks, function(t) {
				return calcEnd(t.start, t.duration);
			}) || 5;
			daysWidth = Math.max(dMax, 20);
			trm.roadMap.taskPanel.xScale().domain([0, daysWidth]);
		}


		function updateLinks(sels) {
			sels.attr("x1", function(e) { return e.source.dim.sx2; });
			sels.attr("x2", function(e) { return e.target.dim.sx1; });
			sels.attr("y1", function(e) { return e.source.dim.syc; });
			sels.attr("y2", function(e) { return e.target.dim.syc; });
			sels.attr("stroke", "silver");
		}

		function updateBlocks(sels) {
			var blockHeight = getTaskRowY(heightFactor) - getTaskRowY(0);
			sels.select("rect").attr("stroke", function(t) {
				return (t.selected) ? "black" : "white";
			})
			.attr("x", function(t,i) { 
				updateTaskPosition(t, i);
				return t.dim.sx1; })
			.attr("y", function(t) { return t.dim.sy1 + topBlockMargin; })
			.attr("height", blockHeight)
			.attr("width", function(t) { return t.dim.sx2 - t.dim.sx1; });
			sels.select("text").attr("x", function(d) {
				return getTimeX(d.start);
			})
			.attr("y", function(d, i) {
				return getTaskRowY(i+heightFactor, d);
			});
			return sels; //for chaining
		}

		function doBlockDrag(d) {
			var x = getTimeX(d.start);
			x += d3.event.dx;
			d.setData("start", getDaysFromX(x));
			if(d3.event.dx>0) {
				traverse.forward(d, 0);
			} else {
				traverse.backward(d);
			}
			updateLinks(vis.selectAll("line.dependency"));
			blocks.call(updateBlocks);
		}

		function doBlockDragEnd(d) {
			traverse.backward(d);
			tasks.forEach(function(t) {
				traverse.forward(t, 0);
			});
			mohio.fire("taskBlocksChanged");
		}
		blockDrag.on("drag", doBlockDrag);
		blockDrag.on("dragend", doBlockDragEnd);

		function doBlockClick(d) {
			if(d3.event.shiftKey && selectedTask) {
				addDependencyLink(selectedTask, d);
			} else {
				if(selectedTask) {
					selectedTask.selected = false;
				}
				selectedTask = d;
				d.selected = true;
			}
			blocks.call(updateBlocks);
			updateLinks(vis.selectAll("line.dependency"));
		}

		function drawTaskTypes() {
			var vis = trm.roadMap.taskPanel.theVis(),
				heads = vis.selectAll("text.taskType").data(tasks);
			trm.roadMap.taskPanel.yScale().domain([0, tasks.length+1]);
			heads.enter().append("text").attr("class", "taskType");
			heads.attr("x", -leftMargin)
			.attr("y", function(d, i) {
				return getTaskRowY(i+heightFactor, d);
			})
			.text(function(d) { return d.type; });//{ return d.title; });
			heads.exit().remove();
			return vis; //chain back to drawTaskBlocks
		}

		function drawTaskBlock(sel) {//expects empty svg:g selection
			sel.append("title").text("Shift-click to add/remove dependencies");
			sel.append("rect").attr("fill", function(d) {
				return trm.getColour(d.index);
			});
			sel.append("text").text(function(d) { return d.title; });
			sel.on("click", doBlockClick);
//			sel.call(blockDrag);
		}
		function drawLinks() {
			var links, edges = trm.graph.edges(function(e) {
				return e.relType==="task-task";
			});
			links = vis.selectAll("line.dependency").data(edges);
			links.enter().append("line")
					.attr("class", "dependency")
					.call(updateLinks);
			links.exit().remove();
		}
		function drawTaskBlocks() {
			blocks = vis.selectAll("g.taskBlock").data(tasks);
			blocks.exit().remove();
			blocks.enter().append("g")
				.attr("class", "taskBlock")
				.call(drawTaskBlock);
			blocks.call(blockDrag);
			blocks.call(updateBlocks);
		}

		function show() {
			tasks = trm.graph.getNodes("task");
			trm.roadMap.taskPanel.yScale().domain([0, tasks.length+1]);
			tasks.sort(function(a,b) {
				return (a.type < b.type) ? -1 : 1;
			});
			vis = drawTaskTypes();
			updateTaskPositions();
			setDisplayDaysWidth();
			drawLinks();
			drawTaskBlocks();
		}		

		trm.roadMap.showTasks = show;
		trm.roadMap.calcEnd = calcEnd;
		trm.roadMap.getTimeX = getTimeX;

		return trm;
	};

	return append;
});
