define(['mohio', 'src/model/ObjectTableModel', 'd3'], function (mohio, makeTableModel) {
	'use strict';
	/*globals d3:false  */
	var append = function (trm) {
		trm.planner = trm.planner || {};
		var resources = {};

		function getResourceNodesTM() {
			var nodes = trm.graph.getNodes("resource");
			return makeTableModel(nodes, ["name","capacity","cost"]);
		}


		function addResourceNode() {
			var tgtGuid, rootNode, index;
			tgtGuid = "Resource"+new Date().getTime();
			rootNode = trm.graph.getNodes("resRoot")[0];//assume 1 node
			index = trm.graph.getNodes("resource").length;
			trm.graph.addNode(tgtGuid, "Resource", "resource",
				{"name": "Res "+index, "capacity": 1, "cost": 1, "index": index}
			);
			trm.graph.addEdge(rootNode.GUID, tgtGuid, "resource");
			trm.planner.show();//refresh the tab
		}

		function addResourcesTable(div, divID) {
			var tm, tbl, div1 = div.append("div").attr("id", divID+"-tableRes");
			div1.append("h4").text("Resources ")
			.append("button").text(" new ").on("click", addResourceNode);
			tbl = mohio.table.make(div1, "matrix");
			tm = getResourceNodesTM();
			mohio.table.populateTableHead(tbl, tm.getHeadings());
			mohio.table.populateTableBody(tbl, tm.getGrid(false));
			tbl.selectAll("td")
			.attr("class", "entry-cell").on("click", function() {
				trm.planner.highlightRow(d3.select(this), tbl);
			})
			.on("dblclick", function(d) {
				trm.planner.updateCell(d3.select(this), d, "Update the cell");
			});
			resources.tbl = tbl;
		}


		resources.addTable = addResourcesTable;

		trm.planner.resources = resources;
		return trm;
	};

	return append;
});
