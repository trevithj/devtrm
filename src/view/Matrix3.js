define([], function () {
	'use strict';
	/*globals d3:false  */
	var append = function (trm) {
		trm.matrix3 = trm.matrix3 || {};

		trm.matrix3.show = function(divID) {
			trm.matrix2.showCombinedMatrix(
				divID, "feature","solution",
				"Matrix3: solution - feature weightings"
			);
		};

		return trm;
	};

	return append;
});
