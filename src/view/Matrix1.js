define(['mohio', 'MatrixGraph', 'd3'], function (mohio, MatrixGraph) {
	'use strict';
	/*globals d3:false  */
	var append = function (trm) {
		trm.matrix1 = trm.matrix1 || {};

		function getDriverAndSegmentNodeEdges() {
			var dNodes = trm.graph.getNodes("driver"),
				sNodes = trm.graph.getNodes("segment"),
				defProps = {visualType:"cell"},
//					function showGUID(key) { return key==="GUID"; };
//					console.log(MatrixGraph.nodesToString(sNodes, showGUID));
//					console.log(MatrixGraph.nodesToString(dNodes, showGUID));
				edges = trm.graph.fullyConnect(sNodes, dNodes, "seg-driver", defProps);
			edges.forEach(function(e) {
				if(e.weight === undefined) {
					e.setData("weight", 1);
				}
			});
			return edges;
		}


		trm.matrix1.show = function (divID) {
			var edges = getDriverAndSegmentNodeEdges(),
				div = mohio.core.clearChildren(divID),
				mx = MatrixGraph.toMatrixObject(edges),
				tbl;
			div.append("h3").text("Matrix1: driver - segment weightings");
			tbl = div.append("table").attr("class", "matrix");
			trm.matrix.writeGrid(mx, tbl);
			tbl.selectAll("td").attr("class", "entry-cell");
		};

		return trm;
	};

	return append;
});
