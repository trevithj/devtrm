define(['mohio','d3'], function (mohio) {
	'use strict';
	/*globals d3:false  */
	var append = function (trm) {
		var mapVType="solution"; //divID = trm.divIDs[6],
		trm.mindMap3 = trm.mindMap3 || {};
		trm.mindMap3.view = null; //to be set externally


		function updateCell(tr, node) {
			tr = d3.select(tr);
			node = node || tr.data()[0]; //.__data__;
			var newName = mohio.core.prompt("Enter a new title for the solution", node.title);
			if(newName) {
				node.setData("title", newName);
				tr.select("td.col0").text(newName);
			}
		}

		function refreshTable() {
			return trm.mindMap.refreshTable(trm.mindMap3.view, "Solutions", mapVType, updateCell);
		}


		function addSolution() {
			var node, guid = "Solution",
				index = trm.graph.getNodes(mapVType).length+1;
			node = trm.graph.addNode(guid+"."+index, "Sol"+index, mapVType);
			node.setData("index", index-1);
			refreshTable();
		}


		trm.mindMap3.show = function() {
			var mmap = trm.mindMap, nodes, edges,
				panel = trm.mindMap3.view.panel;
			panel.theVis().selectAll("g").remove();
			trm.mindMap.panel = panel;
			nodes = mmap.getMapNodes("map3Root");
			edges = mmap.getMapEdges(["map-text3"]);
			trm.mindMap.edges = edges;
			
			mmap.showMap(panel, nodes.all, edges)
			.on("click", function(d) {
				if (d3.event.shiftKey) {
					trm.mindMap.addChildNode(d, "map-text3", panel);
				} else {
					trm.mindMap.handleNodeClick(d);
				}
				mohio.fire("map3NodeChanged");
			})
			.call(trm.mindMap.drag);			

			refreshTable().tableDiv.select("button").text("Add Solution")
			.on("click", addSolution);
		};

		mohio.bind("map3NodeChanged", trm.mindMap3.show);

		return trm;
	};

	return append;
});
