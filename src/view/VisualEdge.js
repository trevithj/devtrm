/* https://github.com/mbostock/d3/wiki/Force-Layout
# force.links([links])

If links is specified, sets the layout's associated links to the specified array.
If links is not specified, returns the current array, which defaults to the empty array.
Each link has the following attributes:
source - the source node (an element in nodes).
target - the target node (an element in nodes).

Note: the values of the source and target attributes may be initially specified as
indexes into the nodes array; these will be replaced by references after the call to start.
Link objects may have additional fields that you specify; this data can be used to compute
link strength and distance on a per-link basis using an accessor function.

 */

define(['d3'], function () {
	'use strict';
	/*globals d3: false */
	var
	LINE_CLASSNAME = 'link',
	LINE_MARKER_NAME = '#arrowHead',
	visualEdge = {};
//	LINK_LABEL_CLASSNAME = 'link-label';

//	function checkAndThrow(value, guid) {
//		if (value === undefined) {
//			throw {"message":"linked node not found: "+guid};
//		}
//	}

	function drawAll(panel, visualEdges) {
		var markerLink,
		lines = panel.theVis().selectAll('.' + LINE_CLASSNAME)
		.data(visualEdges);

		lines.enter()
		.append("line")
//		.insert('svg:line', 'text')
		.attr('class', function(d) {
			return d.visualType + ' ' + LINE_CLASSNAME;
		});

		lines.exit().remove();

		//Reference to SVG definition of arrow head.
		markerLink = 'url(' + LINE_MARKER_NAME + ')';

		lines.attr('x1', function (d) {
			return d.source.x || 0;
		})
		.attr('y1', function (d) {
			return d.source.y || 0;
		})
		.attr('x2', function (d) {
			return d.target.x || 0;
		})
		.attr('y2', function (d) {
			return d.target.y || 0;
		})
		.style('stroke', "silver")
		.style('stroke-width', 2)
		.attr('marker-end', markerLink);//TODO define arrowheads in svg.

		// apparently svg can't change parameters when using svg:def
		//lines.selectAll(GraphPanel.prototype.draw.LINE_MARKER_NAME).attr('fill', function(d) { console.log(d);return d.colour(d.parent); });
	}


	function augment(dataEdge) {

		dataEdge.toString = function () {
			return "[object VisualEdge]";
		};

		return dataEdge;
	}


	/**
	 * Convenience method to augment an array of nodes.
	 * @param {Array} dataEdges
	 * @returns {Array}
	 */
	function convert(dataEdges) {
		dataEdges.forEach(function(dataEdge) {
			augment(dataEdge);
		});
		return dataEdges;
	}

	visualEdge.augment = augment;
	visualEdge.convert = convert;
	visualEdge.draw = drawAll;
	
	return visualEdge;
});
