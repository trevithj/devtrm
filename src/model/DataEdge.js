define(['mohio','DataNode'], function(mohio, DataNode) {
	'use strict';
	/*globals d3: false */

	/**
	 * Using the create/augment/return object pattern as described by Crockford.
	 * @param {object} parent object representing the back-end edge data.
	 * @returns {object} augmented child object representing an edge.
	 */
	function DataEdge(parent) {
		if(!parent.sourceGUID || !parent.targetGUID) {
			throw {message:"Required GUID missing in parameters", name:"InvalidParameter"};
		}
//		parent = DataEdge.flattenExtraProps(parent);
		var edge = Object.create(parent); //DataEdge.flattenExtraProps(parent));
		edge = DataEdge.addDefaultsIfNeeded(edge);

		//Explicitly reads properties of the parent object
		edge.getData = function(name) {
			return parent[name];
		};
		//Explicitly sets properties in the parent object
		edge.setData = function(name, value) {
			parent[name] = mohio.core.toNum(value, value);
		};
		
		edge.toString = function(){
			return "[object DataEdge]";
		};
		

		edge.dfs = function(visitFn) {
			/*jslint nomen: true */
			var outEdges, tgt = this.target,
				response = visitFn(this);
			if( response !== "visitOK" ) {
				return;
			}
			if( !tgt ) {
				return; //something wrong with this edge
			}
			outEdges = tgt.outEdges || tgt._outEdges;
			if( !outEdges ) {
				return;	//nothing to process
			}
			outEdges.forEach(function(edge) {
				edge.dfs(visitFn);
			});
		};

		edge.check = function() {
			return(edge.source.GUID === edge.sourceGUID && edge.target.GUID === edge.targetGUID);
		};

		edge.toJSON = function() {
			return JSON.stringify(parent);
		};

		return edge;
	}

	//Directly inherit a method.
	DataEdge.flattenExtraProps = DataNode.flattenExtraProps;

	DataEdge.addDefaultsIfNeeded = function(edge) {
		function addIfNotDefined(key, value) {
			if(edge[key] === undefined) {
				edge[key] = value;
			}
		}
		//set default properties
		addIfNotDefined('visible', true);
		addIfNotDefined('relType', 'link');
		addIfNotDefined('visualType', '_default');
		addIfNotDefined('GUID',
			edge.sourceGUID + "-" + edge.relType + "-" + edge.targetGUID
		);
		edge.visible = (edge.visible === false) ? false : true;
		return edge;
	};


	return DataEdge;
});