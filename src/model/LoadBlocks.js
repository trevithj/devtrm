define(['d3'], function() {
	'use strict';
	/*globals d3 */

	function LoadBlocks() {
		var timeMap = d3.map(),
		blockNames = d3.set(),
		ret = {};

		function add(name, load, time) {
			var block = timeMap.get(time) || {time:time};
			block[name] = block[name] || 0;
			block[name] += load;
			timeMap.set(time, block);
			blockNames.add(name);
		}

		function addBlock(name, load, from, till) {
			if(name === "time") {
				throw new Error("Illegal block name");
			} else if (load) {
				add(name, +load, from);
				add(name, -load, till);
			}
			return timeMap;
		}

		function getBlocks(sort) {
			sort = sort || function(a,b) {
				return a.time - b.time;
			};
			var blocks = timeMap.values();
			blocks.sort(sort);
			return blocks;
		}

		function getNames(sort) {
			var names = blockNames.values();
			names.sort(sort);
			return names;
		}

		function getLayer(name) {
			var layerPoints = [], cumulativeY = 0;
			getBlocks().forEach(function(obj) {
				cumulativeY += obj[name] || 0;
				layerPoints.push({x:obj.time, y:cumulativeY, y0:0});
			});
			//return {name:name, layer:layerPoints};
			return layerPoints;
		}

		function getLayers() {
			var layers = [];
			getNames().forEach(function(name) {
				layers.push({
					name:name,
					layer:getLayer(name)
				});
			});
			if(layers.length > 1) {
				layers = LoadBlocks.stack(layers);
			}
			return layers;
		}

		ret.addBlock = addBlock;
		ret.getBlocks = getBlocks;
		ret.getNames = getNames;
		ret.getLayer = getLayer; //test only??
		ret.getLayers = getLayers;
		ret.reset = function() {
			timeMap = d3.map();
			blockNames = d3.set();
		};
		return ret;
	}

	/*** 'STATIC methods'***/
	LoadBlocks.stack = d3.layout.stack()
		.values(function(d) { return d.layer; });


	return LoadBlocks;
});