define(['CoreGraph', 'd3'], function(CoreGraph) {
	'use strict';
	/*globals d3 */

	function objToString(obj, txt, showProps) {
		showProps = showProps || function() { return true; }; //show all
		var key;
		for(key in obj) {
			if(typeof obj[key] !== 'function') {
				if(showProps(key)) {
					txt.push(key+"="+obj[key]+"; ");
				}
			}
		}
		txt.push("\n");
		return txt;
	}

	function edgesToString(edges) {
		var txt = [];
		edges.forEach(function(edge) {
			txt.push("src.GUID=" + edge.source.GUID);	txt.push("; ");
			txt.push("tgt.GUID=" + edge.target.GUID);	txt.push("; ");
			txt = objToString(edge, txt);
		});
		return txt.join("");
	}

	function nodesToString(nodes, props) {
		var txt = [];
		nodes.forEach(function(node) {
			txt = objToString(node, txt, props);
		});
		return txt.join("");
	}

	function MatrixGraph(graph) {
		graph = graph || new CoreGraph();

		graph.asMatrix = function(cellFn) {
			return MatrixGraph.toMatrixObject(graph.edges(), cellFn);
		};

		graph.getEdgesByFieldValue = function(field, values) {
			var edges = graph.edges(function(edge) {
				var containsVal = values.some(function(type) {
					return (edge[field] && edge[field]===type);
				});
				return containsVal;
			});
			return edges;
		};

		function findOrAddEdge(srcID, tgtID, defRelType, defProps) {
			var edge = graph.findLink(srcID, tgtID);
			edge = edge || graph.addEdge(srcID, tgtID, defRelType, defProps);
			return edge;
		}

		graph.fullyConnect = function(srcNodes, tgtNodes, defRelType, defProps) {
			var edges = [];
			srcNodes.forEach(function(src) {
				tgtNodes.forEach(function(tgt) {
					var e = findOrAddEdge(src.GUID, tgt.GUID, defRelType, defProps);
					edges.push(e);
				});
			});
			edges.forEach(function(e) {
				if(e.check() === false) {
					console.dir(e);
				}
			});
			if(edges.length !== (srcNodes.length * tgtNodes.length)) {
				throw new Error("Problem!");
			}
			return edges;
		};

		return graph;
	}

//********* 'Static' methods of MatrixGraph **********
//*** private helper methods ***
	function getFromMap(map, key, makeDefaultFn) {
		if(!map.has(key)) {
			map.set(key, makeDefaultFn());
		}
		return map.get(key);
	}

	function getEdgeMap(map, edgesArray, isSrcMap) {
		edgesArray.forEach(function(edge) {
			var key = (isSrcMap) ? edge.sourceGUID : edge.targetGUID,
				ref = (isSrcMap) ? edge.targetGUID : edge.sourceGUID,
				m = getFromMap(map, key, function() {
				return d3.map();
			});
			m.set(ref,edge);
		});
		return map;
	}


//*** public 'static' methods ***
	MatrixGraph.toSourceMap = function(edgesArray) {
		return getEdgeMap(d3.map(), edgesArray, true);
	};

	MatrixGraph.toTargetMap = function(edgesArray) {
		return getEdgeMap(d3.map(), edgesArray, false);
	};

	function getGUIDs(edgesArray, useSrcIDs) {
		var guidSet = d3.set();
		edgesArray.forEach(function(edge) {
			guidSet.add( (useSrcIDs) ? edge.sourceGUID : edge.targetGUID );
		});
		return guidSet.values();
	}

	MatrixGraph.getSrcGUIDs = function(edgesArray) {
		return getGUIDs(edgesArray, true);
	};

	MatrixGraph.getTgtGUIDs = function(edgesArray) {
		return getGUIDs(edgesArray, false);
	};



	MatrixGraph.getEdgeWeight = function(edge, defaultValue) {
		defaultValue = defaultValue || 0;
		return (edge) ? (edge.weight || defaultValue) : defaultValue;
	};

	function mappableEdgeGUID(src, tgt) {
		return src.GUID + "---" + tgt.GUID;
	}

	function buildMatrix(edgesArray, mx) {
		var srcNodeMap = d3.map(),
			tgtNodeMap = d3.map();
		edgesArray.forEach(function(edge) {
			srcNodeMap.set(edge.source.GUID, edge.source);
			tgtNodeMap.set(edge.target.GUID, edge.target);
			var guid = mappableEdgeGUID(edge.source, edge.target);
			mx.edgeMap.set(guid, edge);
//			mx.log.push(guid);
			mx.log.push(guid + "; " + edge.sourceGUID + ":" + edge.targetGUID);
		});
		mx.srcNodes = srcNodeMap.values();
		mx.tgtNodes = tgtNodeMap.values();
		if(mx.edgeMap.values().length !== edgesArray.length) {
			console.log(153, "Warning: possible duplicate edges");
			console.log(mx.edgeMap.values().length, edgesArray.length);
		//	console.log(mx.log);
			console.log(mx.log2);
		}
		return mx;
	};

	/**
	 *
	 * @param {type} edgesArray
	 * @returns {_L1.MatrixGraph.toMatrixObject.mx}
	 */
	MatrixGraph.toMatrixObject = function(edgesArray) {
		var mx = {
			edgeMap: d3.map(),
			log: [],
			srcNodes: null,
			tgtNodes: null
		};
		mx = buildMatrix(edgesArray, mx);

		/**
		Returns a grid as an array of arrays (array[rows][cols]) that contain
		computed values based on any existing links. The computation is handled by the
		provided callback function 'cellFn' that takes an edge object. Note: the function
		should handle the case when an edge object is null/undefined, since this object
		doesn't assume a fully-connected graph.
		If cellFn is omitted, the method returns edge.weight, or 0 if no edge exists.
		To control the position of rows/cols in the grid, sort the src/tgt node arrays appropriately first.
		* @param {type} cellFn takes an edge parameter, returns desired grid contents.
		* @returns {Array} matrix in form [rows][cols].
	   */
		mx.grid = function(cellFn) {
		//	cellFn = (typeof cellFn === 'function') ? cellFn : MatrixGraph.getEdgeWeight;
			cellFn = cellFn || MatrixGraph.getEdgeWeight;
			var grid = [];
			mx.tgtNodes.forEach(function(tgt, r) {
				grid.push([]);
				mx.srcNodes.forEach(function(src) {
					var edge = mx.edgeMap.get(mappableEdgeGUID(src, tgt));
					grid[r].push(cellFn(edge));
				});
			});
			return grid;
		};

		mx.transpose = function(cellFn) {
			cellFn = cellFn || MatrixGraph.getEdgeWeight;
			var grid = [];
			mx.srcNodes.forEach(function(src, r) {
				grid.push([]);
				mx.tgtNodes.forEach(function(tgt) {
					var edge = mx.edgeMap.get(mappableEdgeGUID(src, tgt));
					grid[r].push(cellFn(edge));
				});
			});
			return grid;
		};

		function getNodeHeadings(nodeArray, headFn) {
			var heads = [];
			headFn = headFn || function(node) { return node.GUID; };
			nodeArray.forEach(function(node, i) {
				heads.push(headFn(node, i));
			});
			return heads;
		}

		/**
		 * Fetches an array of desired column headings.
		 * @param {type} headFn [optional] takes a node parameter, returns string.
		 * @returns {Array} strings that represent column headings.
		 */
		mx.colHeads = function(headFn) {
			return getNodeHeadings(mx.srcNodes, headFn);
		};

		/**
		 * Fetches an array of desired row headings.
		 * @param {type} headFn [optional] takes a node parameter, returns string.
		 * @returns {Array} strings that represent row headings.
		 */
		mx.rowHeads = function(headFn) {
			return getNodeHeadings(mx.tgtNodes, headFn);
		};

		mx.column = function(colIndex, cellFn) {
			var col = [], src = mx.srcNodes[colIndex];
			if(src) {
			//	cellFn = (typeof cellFn === 'function') ? cellFn : MatrixGraph.getEdgeWeight;
				cellFn = cellFn || MatrixGraph.getEdgeWeight;
				mx.tgtNodes.forEach(function(tgt) {
					var edge = mx.edgeMap.get(mappableEdgeGUID(src, tgt));
					col.push(cellFn(edge));
				});
			}
			return col;
		};

		return mx;
	};


	MatrixGraph.defaultEdgeGUID = CoreGraph.defaultEdgeGUID;
	MatrixGraph.edgesToString = edgesToString;
	MatrixGraph.nodesToString = nodesToString;

	return MatrixGraph;
});
