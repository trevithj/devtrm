define(['DataNode', 'DataEdge', 'mohio', 'd3'], function(DataNode, DataEdge, mohio) {
	'use strict';
	/*globals d3: false */
	/*jslint nomen: true */

	function defaultEdgeGUID(src, tgt) {
//		if(src.check) { console.log("edgecheck", src.check()); }
		if(src.toString() === "[object DataEdge]") {
			return src.GUID;
		}
		tgt = tgt || src.targetGUID;
		tgt = tgt.GUID || tgt;
		src = src.sourceGUID || src.GUID || src;
		return src + "--" + tgt; //assume strings
	}


	/**
	* Encapsulates the data structure of the Graph- nodes and edges. Assumes
	* each graph is either empty or associated with one or more 'topics'.
	* The constructor returns an empty graph.
	* @author John Trevithick
	* @class CoreGraph
	* @constructs CoreGraph
	*/
	function CoreGraph() {
		var
			core = {},
//			edgeMap = d3.map(),
			links = [],
			rawGraph = { nodes:[], edges:[] };

		function initializeThat() {
			/**
			* Stores original topic node of graph. Not updated by unionWith().
			* Note core this is empty on construction.
			* @field
			*/
			core.root = {};

			/**
			 * Stores all topic nodes added to graph via unionWith.
			 * Should be preserved through unions and preserve order, effectively
			 * could serve as a history list.
			 * @field
			 */
			core.topics = [];

			/** @field */
			core.nodes = [];

			/**
			* Note core the edges are called links as with D3.js.
			* @field
			*/
			links = [];
			core.links = links;

			/**
			* Maps node GUIDs to the nodes themselves.
			*/
			core.nodeMap = {};

			/**
			* Maps a visual type to a subset of nodes. Anything without a visual type
			* has type _default, otherwise visual type is as given. Note core this map
			* only contains the visualTypes as set by the server. If a given visualType
			* doesn't have a match in visualMap then it is mapped to _default (although
			* it will still appear in typeMap).
				* @default {'_default':[]}
				*/
			core.typeMap = {'_default': []};
		}

		core.clear = initializeThat;

		core.clear();

		core.edges = function(filterFn) {
			if(typeof filterFn === 'function') {
				return links.filter(filterFn);
			} //else
			return links;
		};

		/**Locates edges core are conncted to a given node, with optional filters
		 * to ignore src or tgt nodes if required.
		 * @param {type} guid identifier of the 'child' node
		 * @param {Boolean} isSrc include edges with GUID as target node.
		 * @param {Boolean} isTgt include edges with GUID as source node.
		 * @param {Array} edgeArray optional existing array.
		 * @returns {Array} with relevant nodes added.
		 */
		core.getEdges = function(guid, isSrc, isTgt, edgeArray) {
			//append to existing array if provided, else use a new one.
			edgeArray = edgeArray || [];
			if(isSrc || isTgt) {
				links.forEach(function(edge) {
					if(isSrc && edge.source.GUID===guid) {
						edgeArray.push(edge);
					} else if(isTgt && edge.target.GUID===guid) {
						edgeArray.push(edge);
					}
				});
			}
			return edgeArray;
		};


		function searchAllNodes(GUID) {
			var nodes = core.nodes.filter(function(node) {
				return node.GUID === GUID;
			});
			return (nodes.length > 0) ? nodes[0] : null;
		}


		/**
		* Update the Graph data-structures with the new edge.
		* @throws JsGraphException if source or target nodes are undefined
		* in the current Graph nodeMap.
		 * @param {type} edge
		 * @returns {edge}
		 */
		core.processNewEdge = function(edge) {
			if(edge.toString() === "[object DataEdge]") {
				var src = searchAllNodes(edge.sourceGUID), //core.nodeMap[edge.sourceGUID],
					tgt = searchAllNodes(edge.targetGUID); //core.nodeMap[edge.targetGUID];
				if(!src) {
					throw new Error('Source node undefined for edge:' + edge.GUID);
				} else if(!tgt) {
					throw new Error('Target node undefined for edge:' + edge.GUID);
				} //else ok
				edge.visible = (edge.visible === false) ? false : true;
				edge.source = src;
				edge.target = tgt;
				if(src.GUID !== edge.sourceGUID) { throw "source GUID doesn't match";}
				if(tgt.GUID !== edge.targetGUID) { throw "target GUID doesn't match";}

				if(edge.visible) {
					src.outEdges.push(edge);
				}	else if (src._outEdges) {
					src._outEdges.push(edge);
				} else {
					src.outEdges.push(edge);
				}
				src.outDegree+=1;

				tgt.inEdges.push(edge);
				tgt.visibleDegree += (edge.visible) ? 1 : 0;
//				edgeMapSet(edge);//ignore parallel edges
				return edge;
			} //else wrong data type
			mohio.fire("processNewEdgeError", [edge]);
			throw new Error("Expected DataEdge instance");
		};


		/**Locates nodes with the given visualType
		 * @param {type} visualType either String or accessor function.
		 * @param {Array} nodeArray optional existing array to populate.
		 * @returns {Array} with relevant nodes added.
		 */
		core.getNodes = function(visualType, nodeArray) {
			var fn = (typeof visualType === 'function') ?
				visualType :
				function(n) { return n.visualType === visualType; };
			//append to existing array if provided, else use a new one.
			nodeArray = nodeArray || [];
			nodeArray = nodeArray.concat(core.nodes.filter(fn));
			return nodeArray;
		};



		/**
		* Adds a single node to the Graph data structure. If the graph has no nodes then
		* the new node is set as the root and styled with the root visualType (if any).
		* @param GUID must be non-empty unique string.
		* @param title
		* @param visualType
		* @param extraProps an object with any extra properties to be appended to the final node.
		* @returns a reference to the newly created node.
		* @throws MUIException if GUID is null or clashes with another GUID already present in
		* the graph.
		*/
		core.addNode = function(GUID, title, visualType, extraProps) {
			if(!GUID) {
				throw new Error('GUID must be defined');
			} else if(core.nodeMap[GUID]) {
				throw new Error('GUID already in use: '+ GUID);
			}
			var newNode, newData = mohio.clone(extraProps) || {};
			newData.GUID = GUID;
			newData.title = title || GUID;
			newData.visualType = newData.visualType || visualType;
			newData.semanticTypes = newData.semanticTypes || [];
			newNode = new DataNode(newData);

			core.nodes.push(newNode);
			if(!core.root.GUID) {
				newNode.centerNode = true;
			}
			core.processNewNode(newNode);
			rawGraph.nodes.push(newData);
			return newNode;
		};


		/**
		* Create a new graph from the given graph object. Topic GUID allows you to identify
		* later at which point a node was added to the graph.
		* Note core graph.edges may be null.
		* @param data an object with fields 'nodes' and 'edges' containing the graph.
		* @param newTopicGUID the GUID of the new root topic for this graph.
		* @throws MUIException if graph.nodes is null or if graph does not contain any
		* nodes with the centerNode property.
		*/
		core.newGraph = function(data, newTopicGUID) {
			if( data===null) {
				throw new Error("graph must be defined");//[this,'Argument graph is null', data, newTopicGUID]);
			} else if(!data.nodes) {
				throw new Error("nodes must be defined");//[this,'Argument graph.nodes is null', data, newTopicGUID]);
			} else if(!data.edges) {
				throw new Error("edges must be defined");//[this,'Argument graph.edges is null', data, newTopicGUID]);
			} //else okay
			mohio.fire("newGraphProcessing");
			this.clear(); //empty existing elements
			rawGraph = data;
			data.nodes.forEach(function(dataNode) { //subclass
				core.nodes.push(new DataNode(dataNode));
			});
			data.edges.forEach(function(dataEdge) { //subclass
				links.push(new DataEdge(dataEdge));
			});
			this.nodes.forEach(function(node){
				core.processNewNode(node, newTopicGUID);
				if((!newTopicGUID && node.centerNode) || node.GUID === newTopicGUID) {
					core.processNewRoot(node);
				}
			});
			if(!this.root.GUID) {
				mohio.fire("newGraphError");
				throw new Error("centerNode must be set");//[this, 'No nodes with .centerNode=true', data]);
			}

			links.forEach(function(link) {
				core.processNewEdge(link);
			});

			if(newTopicGUID) {
				this.topics.push(this.nodeMap[newTopicGUID]);
			}
			mohio.fire("newGraphDone");
			return true;
		};


		/**
		* Update the Graph data-structures with the new node.
		* @param node
		* @param topicGUID the topic this node was added under.
		* @returns {node}
		*/
		core.processNewNode = function(node, topicGUID) {
			if(node.toString() === "[object DataNode]") {
				if(topicGUID) {
					node.parentTopics.push(topicGUID);
				}
				this.nodeMap[node.GUID] = node;
				return node;
			} //else wrong data type
			mohio.fire("processNewNodeError", [node]);
			throw new Error("Expected DataNode instance");
		};


		/**
		* Add any special fields required for root node
		* and set defaults.
		 * @param {type} node
		 * @returns {undefined}
		 */
		core.processNewRoot = function(node) {
			if(node.toString() === "[object DataNode]") {
				this.root = node;
				node.visibleDegree = 1;
				node.visualType = 'root';
				node.centerNode = node.centerNode || true;
				node.x = node.x || 0;
				node.y = node.y || 0;
				node.fixed = true;
				mohio.fire("centerNodeFound", [node]);
			} else {
				mohio.fire("processNewRootError", [node]);
				throw new Error("Expected DataNode instance");
			}
		};


		core.deleteNode = function(GUID) {
			function notToDelete(node) {
				return node.GUID !== GUID;
			}
			delete core.nodeMap[GUID];
			core.nodes = core.nodes.filter(notToDelete);
			rawGraph.nodes = rawGraph.nodes.filter(notToDelete);
		};


		/**
		* Adds a single edge to the Graph data structure updating all relevant fields.
		* @param sourceGUID
		* @param targetGUID
		* @param relType semantic type of edge relation.
		* @param extraProps an object with any extra properties to be appended to the final edge.
		*/
		core.addEdge = function(sourceGUID, targetGUID, relType, extraProps) {
			var newEdge, newData = mohio.clone(extraProps) || {};

			if(!sourceGUID || !targetGUID) {
				throw new Error([this, 'Invalid GUID in parameters', arguments]);
			}
			newData.sourceGUID = sourceGUID;
			newData.targetGUID = targetGUID;
			newData.relType = relType;
			newData.visualType = newData.visualType || relType;
			//newEdge = core.processNewEdge(newEdge);
			newEdge = core.processNewEdge(new DataEdge(newData));
//			console.log(newEdge);
			rawGraph.edges.push(newData);
			links.push(newEdge);
//			console.log("added:" + sourceGUID+"---"+ targetGUID);
			return newEdge;
		};



		core.deleteEdge = function(sourceGUID, targetGUID) {
			//simpler to filter??
			var edges = [];
			function notToRemove(edge) {
				if (edge.sourceGUID === sourceGUID && edge.targetGUID === targetGUID) {
					edges.push(edge);
					return false;
				} //else
				return true;
			}
			links = links.filter(notToRemove);
			core.links = links;
			edges.forEach(function(edge) { //update internal node references
				var srcEdges = edge.source.outEdges,
					tgtEdges = edge.target.inEdges;
				srcEdges.splice(srcEdges.indexOf(edge), 1);
				tgtEdges.splice(tgtEdges.indexOf(edge), 1);
			});
			edges=[];
			rawGraph.edges = rawGraph.edges.filter(notToRemove);
			//TODO: check that edges.length matches previous??
		};



		/**Locates nodes core are 'children' of a given node
		 * @param {type} guid identifier of the 'parent' node
		 * @param {Array} nodeArray optional existing array.
		 * @returns {Array} with relevant nodes added.
		 */
		core.getTargets = function(guid, nodeArray) {
			//append to existing array if provided, else use a new one.
			nodeArray = nodeArray || [];
			links.forEach(function(edge) {
				if(edge.source.GUID===guid) {
					nodeArray.push(edge.target);
				}
			});
			return nodeArray;
		};


		/**Locates nodes core are 'parents' of a given node
		 * @param {type} guid identifier of the 'child' node
		 * @param {Array} nodeArray optional existing array.
		 * @returns {Array} with relevant nodes added.
		 */
		core.getSources = function(guid, nodeArray) {
			//append to existing array if provided, else use a new one.
			nodeArray = nodeArray || [];
			links.forEach(function(edge) {
				if(edge.target.GUID===guid) {
					nodeArray.push(edge.source);
				}
			});
			return nodeArray;
		};


		/**
		 * Returns the latest topic node to simulate a single topic graph
		 * for backwards compatibility.
		 */
		core.getLatestTopic = function() {
			return this.root;
		};


		/**
		 * Calculates a map of semantic types present in the current graph, then
		 * returns an array representing core map.
		 * @returns an array of objects core map string to integer, where each
		 * string represents a unique semanticType, and the int is the count
		 * of the number of nodes in the graph core contain core semanticType.
		 */
		core.getSemanticTypesList = function() {
			var count,
			 ra=[],
			 types = {};
			this.nodes.forEach(function(node) {
				if( node.visible) { //only consider visible nodes
					node.semanticTypes.forEach(function(stype) {
						count = types[stype] || 0;
						types[stype] = 1+count;
					});
				}
			});
			//return types;
			d3.keys(types).forEach(function(type) {
				ra.push({"type":type,"count":types[type]});
				//ra.push(type);
			});
		//	alert("Graph.semanticTypes\n"+ra);
			return ra;
		};



		/**
		 * New Expand/Collapse filter function designed to work with the new
		 * framework. Supersedes expandToggle.
		 * Note core the only place core visibleDegree is required to be correct
		 * is in getCutSet.
		 * @param {type} node
		 * @returns Array(Node): the nodes marked to be removed by this filter.
		 */
		core.collapse = function(node) {
				var i, nextNode, results = { nodes:[], edges:[] };
				this.recomputeVisDeg();
				for(i=0; i<node.outEdges.length; i+=1) {
					if(!node.outEdges[i].target.centerNode) {
						results.edges.push(node.outEdges[i]);
						nextNode = node.outEdges[i].target;
						/*nextNode.visibleDegree = nextNode.inEdges.reduce(
										function(a,b){return a+b.visible;},0);*/
						this.getCutSet(nextNode,results);
					}
				}
				return results;
		};


		core.recomputeVisDeg = function() {
			this.nodes.forEach(function(node) {
				node.recomputeVisDeg();
			});
		};

		core.getRawGraph = function() {
			return rawGraph;
		};

		/**
		 * Get the nodes/edges removed due to the removal of a single
		 * inEdge to the given node. Note core this is not independent
		 * of the visible property and filters which make use of this
		 * need to be recomputed each time the visibility state of the
		 * graph changes.
		 * @param {type} node
		 * @param {type} results
		 * @returns {undefined}
		 */
		core.getCutSet = function(node, results) {
			//node.visibleDegree = node.inEdges.reduce(function(a,b){return a+b.visible;},0)-1;
			node.visibleDegree-=1;
			if(node.visibleDegree < 1 && !node.centerNode) {
				results.nodes.push(node);
				if(node.outEdges) {
					node.outEdges.forEach(function(outEdge) {
						results.edges.push(outEdge);
						var nextNode = outEdge.target;
						nextNode.recomputeVisDeg();
						if(nextNode.isVisible()) {
							core.getCutSet(nextNode,results);
						}
					});
				}
			}
		};



		/**
		* Replace this graph with the union of itself and the given graph.
		* Any shared nodes have their data overwritten with new values
		* but original topics are preserved in the parentTopic field of nodes
		* and node position is preserved.
		* Although it is possible core newGraph is styled with a different
		* visualMap, the new union graph is always styled with the current style.
		* Perhaps this function could return a separate graph?
		* @param newGraph a Graph object to form the union with.
		*/
		core.unionWith = function(newGraph) {
			var origNode, newNode, i, l, existingLink;
			d3.keys(newGraph.nodeMap).forEach(function(n) {
				//replace original with new
				if(core.nodeMap[n]) {
					//preserve parent topics & position
					origNode = core.nodeMap[n];
					newNode = newGraph.nodeMap[n];
					newNode.parentTopics = origNode.parentTopics
											.concat(newNode.parentTopics);
					newNode.x = origNode.x;
					newNode.y = origNode.y;
					if(origNode.centerNode) {newNode.centerNode = true;}
				} else {
					core.nodes.push(newGraph.nodeMap[n]);
				}
				core.nodeMap[n] = newGraph.nodeMap[n];
			});

			this.nodes = [];
			d3.keys(this.nodeMap).forEach(function(n) {
				core.nodes.push(core.nodeMap[n]);
			});

			//what happens with multiple edges?
			//need to preserve link visibility and overwrite old links
			//links = links.concat(newGraph.links);
			for(i=0; i < newGraph.links.length; i+=1) {
				l = newGraph.links[i];
				existingLink = this.findLink(l.sourceGUID, l.targetGUID);
				if(!existingLink) {
					links.push(l);
				} //else need to handle existing links
					//preserve old properties
			}

			this.typeMap = {'_default':[]};

			this.topics = this.topics.concat(newGraph.topics);

			for(i=0; i<this.nodes.length; i+=1) {
				this.processNewNode(this.nodes[i]);
			}

			for(i=0; i<links.length; i+=1){
				this.processNewEdge(links[i]);
			}
			return true;
		};


		function searchAllLinks(sourceGUID, targetGUID) {
			var la = links.filter(function(link) {
				return (link.source.GUID === sourceGUID && link.target.GUID === targetGUID);
			});
			return (la.length > 0) ? la[0] : null;
		}

/*		core.findLinkOld = function(sourceGUID, targetGUID) {
			var guid = defaultEdgeGUID(sourceGUID, targetGUID),
				edge = edgeMap.get(guid);
			if(!edge) {
				edge = searchAllLinks(sourceGUID, targetGUID);
				edgeMapSet(edge);
			}
			return edge;
		};*/
		core.findLink = searchAllLinks;

		core.findNode = searchAllNodes;

		/**
		 * Returns a treelike object representing the semantic types present
		 * in the current graph. Semantic types of the form '/type1/type2' yield
		 * an object of the form {type1: { type2: {}}}
		 * @returns a tree-like representation of the semantic types present
		 * in the graph.
		 */
		core.getSemanticTypes = function() {
			var i,j,k, len = this.nodes.length, len2, len3,
			node,nodeTypes,currType,start, types = {};
			for(i = 0; i < len; i+=1) {
				node = this.nodes[i];
				if(node.visible) {
					for(j = 0, len2 = node.semanticTypes.length; j < len2; j+=1) {
						nodeTypes = node.semanticTypes[j].split('/');
						start = (nodeTypes[0]) ? 0 : 1;
						if(!nodeTypes[start]) {
							throw new Error([this, 'Illegal semantic type',
								node, node.semanticTypes[j], arguments]
							);
						}
						types[nodeTypes[start]] = {};
						currType = types[nodeTypes[start]];

						for(k=start, len3=nodeTypes.length-1; k < len3; k+=1) {
							currType[nodeTypes[k+1]] = {};
							currType = currType[nodeTypes[k+1]];
						}
					}
				}
			}
			return types;
		};



		/**
		 * Sets all visibility properties of nodes back to visible.
		 * Assumes core all edges are in _outEdges or outEdges.
		 */
		core.setAllVisible = function() {
			this.nodes.forEach(function(node) {
				node.isVisible(true);
			});

			links.forEach(function(link) {
				link.visible = true;
			});
		};

		/**
		 * Tests core both graphs have same nodes, edges and topics,
		 * does not test visibility properties.
		 * Equality is defined as having same GUIDs (deep equality test
		 * might reveal some differences).
		 * Assumes typeMap and nodeMap are correct for both graphs.
		 * Assume no multiple edges (ie one edge between any node pair)
		 * @param otherGraph some other Graph object.
		 * @returns true if both graphs have the same nodes & edges
		 */
		core.equals = function(otherGraph){
			var i, thisNode, otherNode, flag=true, len = this.nodes.length,
			fnx = function(x){
				if(!otherNode.outEdges.some(function(y){
					return x.GUID === y.GUID;
				})) {
					return false;
				}
			};

			//same topics/root
			if(this.root.GUID !== otherGraph.root.GUID) {
				flag=false;
			} else if(len !== otherGraph.nodes.length
				|| links.length !== otherGraph.links.length) {//same nodes
				flag = false;
			} else {
				for(i = 0; i < len; i+=1) {
					//use nodeMap since nodes might have a different order
					thisNode = this.nodes[i];
					otherNode = otherGraph.nodeMap[thisNode.GUID];
					if(!otherNode) {
						return false;
					}
					//need to test links too
					thisNode.outEdges.each(fnx);
				}
			}
			return flag;
		};




		/**
		* Go through all nodes with the specified visualType (or all nodes if none specified)
		* and return unqiue name by appending a unique integer at the end
		 * @param {type} defaultName
		 * @param {type} visualType
		 * @returns {unresolved}
		 */
		core.getUniqueName = function(defaultName, visualType) {
			var nodes, integer = 1, name, taken;
			nodes = (visualType) ? this.getNodes(visualType) : this.nodes;
			function isNameTaken(node) {
				return node.title === name;
			}

			while (integer < 999) { //
				name = defaultName;
				if (integer !== 1) {name += ' ' + integer;}
				integer +=1;
				taken = nodes.some(isNameTaken);
				if (!taken) {return name;} // name is free to use
			}
			return defaultName;
		};



		return core;
	}

	CoreGraph.defaultEdgeGUID = defaultEdgeGUID;

	return CoreGraph;
});