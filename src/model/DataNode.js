define(['mohio'], function(mohio) {
	'use strict';
	/*globals d3: false */
	/**
	 * Using the create/augment/return object pattern as described by Crockford.
	 * @param {object} parent object representing the back-end node data.
	 * @returns {object} augmented child object representing a node.
	 */
	function DataNode(parent) {
		if(!parent.GUID) {
			throw {message:"GUID must be defined", name:"InvalidParameter"}; //{"",""};
		}
//		parent = DataNode.flattenExtraProps(parent);
		var node = Object.create(DataNode.flattenExtraProps(parent));
		node = DataNode.addDefaultsIfNeeded(node);

		/**Compare two GraphNode objects
		 * Equality is defined as having same GUIDs (deep equality test
		 * might reveal some differences).
		 * @param {type} otherNode
		 * @returns {undefined}
		 */
		node.compareNodes = function(otherNode) {
			//TODO: implement check of edges?
			return node.GUID === otherNode.GUID;
		};

		/** Tests or sets node visibility.
		* @param {type} flag controls how this function responds.
		* if true, all visibility properties of the node are set to visible.
		* if false/omitted, returns boolean indicating visibility status.
		* @returns {Boolean}
		*/
		node.isNodeVisible = function(flag) {
			if(flag) {	//set this node as visible
				node.visible = true;
				//root nodes should never be hidden
				node.visibleDegree = (node.centerNode) ? 100 : node.inEdges.length;
			} else {		//test for node visibility
				flag = (node.visibleDegree > 0 && node.visible);
			}
			return flag;
		};


		node.recomputeNodeVisDeg = function() {
			var visDegree = node.inEdges.reduce(
				function(a,b){return a+b.visible;},	0
			);
			node.visibleDegree = visDegree;
		};

		//Explicitly reads properties of the parent object
		node.getData = function(name) {
			return parent[name];
		};
		//Explicitly sets properties in the parent object
		node.setData = function(name, value) {
			parent[name] = mohio.core.toNum(value, value);
		};

		//In order to positively identify this object type,
		//do a string compare rather than instanceof.
		node.toString = function() {
			return "[object DataNode]";
		};

		node.zIndex = function() {
			return node.zIndex === undefined ? 0 : node.zIndex;
		};



		node.dfs = function(visitFn) {
			/*jslint nomen: true */
			var outEdges,
				response = visitFn(this);
			if( response !== "visitOK" ) {
				return;
			}
			outEdges = node.outEdges || node._outEdges;
			if( !outEdges ) {
				return;	//nothing to process
			}
			outEdges.forEach(function(edge) {
				edge.target.dfs(visitFn);
			});
		};

		node.toJSON = function() {
			return JSON.stringify(parent);
		};




		return node;
	}

	DataNode.addDefaultsIfNeeded = function(node) {
		function addIfNotDefined(key, value) {
			if(node[key] === undefined) {
				node[key] = value;
			}
		}
		//set default properties
		addIfNotDefined('outDegree', 0);
		addIfNotDefined('outEdges', []);
		addIfNotDefined('_outEdges', []);
		addIfNotDefined('visibleDegree', 0);
		addIfNotDefined('inEdges', []);
		addIfNotDefined('visible', true);
		addIfNotDefined('parentTopics', []);
		addIfNotDefined('visualType', '_default');
		addIfNotDefined('x', Math.round(1000 * Math.random())/1000);
		addIfNotDefined('y', Math.round(1000 * Math.random())/1000);

		return node;
	};

	DataNode.flattenExtraProps = function(data) {
		if(data.extraProperties) { //assume an object
			var tmp = data.extraProperties;
			d3.keys(data).forEach(function(key) {
				if(key !== "extraProperties") {
					tmp[key] = data[key];
				}
			});
			data = tmp;
		} //else nothing to flatten
		return data;
	};

	return DataNode;
});
