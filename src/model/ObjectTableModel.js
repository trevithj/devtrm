/* 
 * Wraps an array of objects in a set of methods that allow easy conversion
 * to/from a table format
 */

define(['jquery', 'd3'], function ($) {
//pass explicit jquery ref, but d3 isn't a module. Both are defined globally anyway.
	'use strict';
	/*globals d3 */
	
	
	function makeObjectTableModel(dataArray, propsArray) {
		var tm = {};
		
		function makeAccessor(dat, prop) {
			var fn = {};
			fn.data = function() { return dat; };
			fn.get = function() {
				return (dat[prop] === undefined) ? null : dat[prop];
			};
			fn.set = function(val) {
//				dat[prop] = val;
				dat.setData(prop, val);
			};
			fn.toString = function() {
				return "" + fn.get();
			};
			return fn;
		}

		tm.getRow = function(rowIndex, dat) {
			var row = [];
			dat = dat || dataArray[rowIndex];
			propsArray.forEach(function(prop) {
				row.push(makeAccessor(dat, prop));
			});
			return row;
		};

		tm.getHeadings = function() {
			return propsArray;
		};

		tm.getGrid = function(withHeadings) {
			var grid = [];
			if(withHeadings) {
				grid.push(tm.getHeadings());
			}
			dataArray.forEach(function(dat, r) {
				grid.push(tm.getRow(r, dat));
			});
			return grid;
		};

		tm.getValue = function(rowIndex, colName) {
			var data = dataArray[rowIndex];
			//check that colName exists in propsArray?
			return makeAccessor(data, colName);
		};

		return tm;
	}


	return makeObjectTableModel;
});

